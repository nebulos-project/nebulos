/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "shared/utilities.hpp"

#include <boost/algorithm/string.hpp>

using namespace std;

namespace nebulos
{

bool IsCommandString(const string& _command)
{
   return (_command[0] == static_cast<char>(200) &&
           _command[1] == static_cast<char>(210) &&
           _command[2] == static_cast<char>(222) &&
           _command[3] == static_cast<char>(234));
}


StringList MakeCommandList(const string& _commands)
{
   StringList commands;

   boost::split(commands,
                _commands,
                boost::algorithm::is_any_of("\n;"),
                boost::algorithm::token_compress_on);

   if (IsCommandString(_commands))
   {
      commands[0].erase(0,4);  // remove the magic number from the string
   }

   return commands;
}



} // ns nebulos
