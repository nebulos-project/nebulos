/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DFS_TOOLS_HPP
#define DFS_TOOLS_HPP

#include "shared/nebulos.hpp"

#include <utility>
#include <unordered_map>

struct hdfs_internal;
typedef struct hdfs_internal* hdfsFS;

namespace nebulos {

typedef std::vector<std::pair<std::string, float> > SlaveFractionList;

typedef std::unordered_map<std::string, int> StringIntMap;


/**
 * @brief The DfsTools class provies an interface to the HDFS file system.
 *
 * This class handles the lower level details of HDFS management.
 * It does this my managing an hdfs_internal pointer and wrapping many
 * of the functions available in libHDFS (hdfs.h).
 */
class DfsTools
{
public:

   DfsTools()
   {
      Connect();
   }

   DfsTools(const std::string& namenode)
      : namenode_(namenode)
   {
      Connect();
   }

   ~DfsTools()
   {
      Disconnect();
   }

   DfsTools(const DfsTools& other) = delete;
   DfsTools(DfsTools&& other) = delete;
   DfsTools& operator=(const DfsTools& other) = delete;
   DfsTools& operator=(DfsTools&& other) = delete;

   /**
    * @brief mesos_master sets the Mesos master
    * @param master is the address:port-number of the Mesos master.
    */
   void mesos_master(const std::string& master)
   {
      mesos_master_ = master;
   }

   /**
    * @brief mesos_master informs user of the current mesos-master, used by the class
    * @return the hostname:port of the mesos-master.
    */
   std::string mesos_master() const { return mesos_master_; }

   /**
    * @brief QueryMesos updates the mapping between hostnames and Mesos slave
    * IDs. This needs to be done when an event occurs that may change the ID
    * of a slave.
    * @param master is the address:port-number of the Mesos master.
    */
   void QueryMesos(const std::string& master)
   {
      mesos_master_ = master;

      hosts_to_slaves_ = HostnamesToSlaveIDs();
   }

   /**
    * @brief GetSlaveID finds the mesos slave id, given the hostname of the
    * machine of interest.
    * @param hostname
    * @return The Slave_ID associated with the host named hostname.
    */
   std::string GetSlaveID(const std::string& hostname);

   /**
    * @brief GetHosts finds which hosts contain the blocks of a specified file
    * on the DFS.
    * @param file is the full path to the file of interest.
    * @return The SlaveFractionList returned contains one hostname-fractional
    * ownership pair, sorted from highest to lowest ownership fraction. If the
    * entire file is contained on the host, the ownership fraction is 1.0. A
    * value of 0.1 indicates that one tenth of the file is local to the host.
    */
   SlaveFractionList GetHosts(const std::string& file);

   /**
    * @brief GetHosts finds which hosts contain the blocks containing a group
    * of files in the DFS.
    * @param files a list of file names (full paths to the files of interest).
    * @return The SlaveFractionList returned contains one hostname-fractional
    * ownership pair, sorted from highest to lowest ownership fraction. If all
    * of the data associated with the list of files is contained on a single
    * host, the ownership fraction for that slave is 1.0. A  value of 0.1
    * indicates that one tenth of the data is local to the host.
    */
   SlaveFractionList GetHosts(const nebulos::StringList& files);

   /**
    * @brief GetSlaves finds which slaves contain the blocks of a specified file
    * on the DFS.
    * @param file is the full path to the file of interest.
    * @return The SlaveFractionList returned contains one slavename-fractional
    * ownership pair, sorted from highest to lowest ownership fraction. If the
    * entire file is contained on the slave, the ownership fraction is 1.0. A
    * value of 0.1 indicates that one tenth of the file is local to the slave.
    */
   SlaveFractionList GetSlaves(const std::string& file);

   /**
    * @brief GetSlaves finds which slaves contain the blocks containing a group
    * of files in the DFS.
    * @param files a list of file names (full paths to the files of interest).
    * @return The SlaveFractionList returned contains one slavename-fractional
    * ownership pair, sorted from highest to lowest ownership fraction. If all
    * of the data associated with the list of files is contained on a single
    * slave, the ownership fraction for that slave is 1.0. A  value of 0.1
    * indicates that one tenth of the data is local to the slave.
    */
   SlaveFractionList GetSlaves(const nebulos::StringList& files);

   /**
    * @brief FileSize finds the size, in bytes, of a specified file.
    * @param filename is the full path to a file, relative to the DFS root
    * (not relative to a mountpoint).
    * @return the size of a file in bytes.
    */
   long int FileSize(const char* filename) const;

   /**
    * @brief FileSize finds the total size of the specified files.
    * @param files is a StringList of filenames.
    * @return the total, combined size of the specified files, in bytes.
    */
   long int FileSize(const nebulos::StringList& files) const;

   /**
    * @brief GetReplicationFactor finds the replication factor of the
    * specified file
    * @param filename is the name of the file of interest.
    * @return The replication factor for the specified file.
    */
   int GetReplicationFactor(const char* filename) const;

   /**
    * @brief SetReplicationFactor sets the replication factor for the
    * specified file.
    * @param filename is the name of the file of interest.
    * @param factor is the number of copies of each file block, for redundancy.
    * @return Returns true on success, false on failure.
    */
   bool SetReplicationFactor(const char* filename, int factor);

   /**
    * @brief GetBlockSize find the block size of the specified file.
    * @param filename is the name of the file of interest
    * @return Returns the block size of the file in bytes.
    */
   long int GetBlockSize(const char* filename) const;

   /**
    * @brief CopyToDfs copies a file from a local filesystem to HDFS
    * @param source the source file on the local filesystem
    * @param dest the destination filename on HDFS.
    * @return Returns true on success, false on failure.
    */
   bool CopyToDfs(const std::string& source, const std::string& dest);

   /**
    * @brief CopyFromDfs copies a file from HDFS to a local filesystem.
    * @param source is the source file in the HDFS.
    * @param dest is the destination filename on the local filesystem.
    * @return Returns true on success, false on failure.
    */
   bool CopyFromDfs(const std::string& source, const std::string& dest);

   /**
    * @brief CopyWithinDfs copies a file from one location in the HDFS to
    * another location within the HDFS.
    * @param source is the source filename.
    * @param dest is the destination filename.
    * @return Returns true on success, false on failure.
    */
   bool CopyWithinDfs(const std::string& source, const std::string& dest);

   /**
    * @brief MoveToDfs moves a file from a local filesystem to HDFS
    * @param source the source file on the local filesystem
    * @param dest the destination filename on HDFS.
    * @return Returns true on success, false on failure.
    */
   bool MoveToDfs(const std::string& source, const std::string& dest);

   /**
    * @brief MoveFromDfs moves a file from HDFS to a local filesystem.
    * @param source is the source file in the HDFS.
    * @param dest is the destination filename on the local filesystem.
    * @return Returns true on success, false on failure.
    */
   bool MoveFromDfs(const std::string& source, const std::string& dest);

   /**
    * @brief MoveWithinDfs moves a file from one location in the HDFS to
    * another location within the HDFS.
    * @param source is the source filename.
    * @param dest is the destination filename.
    * @return Returns true on success, false on failure.
    */
   bool MoveWithinDfs(const std::string& source, const std::string& dest);

   /**
    * @brief Rename is equivalent to MoveWithinDfs()
    * @param source is the source filename.
    * @param dest is the destination filename.
    * @return Returns true on success, false on failure.
    */
   bool Rename(const std::string& source, const std::string& dest);

   /**
    * @brief Remove deletes / removes the specified file.
    * @param filename is the file to be deleted.
    * @return Returns true on success, false on failure.
    */
   bool Remove(const std::string& filename);

   /**
    * @brief RemoveDirectory removes / deletes a directory.
    * @param dirname is the directory to be deleted.
    * @return Returns true on success, false on failure.
    */
   bool RemoveDirectory(const std::string& dirname);

   /**
    * @brief CreateDirectory creates a new directory with the specified name.
    * @param dirname is the name of the directory to be created.
    * @return Returns true on success, false on failure.
    */
   bool CreateDirectory(const std::string& dirname);

protected:

   /**
    * @brief Connect to the Hadoop Distributed File System.
    */
   void Connect();

   /**
    * @brief Disconnect from the Hadoop Distributed File System.
    */
   void Disconnect();

   /**
    * @brief StripPrefix removes parent directories from a file path until the
    * root of the DFS is found. This handles the case in which the file system is
    * mounted in the local filesystem.
    * @param file is the full path to a file.
    * @return Returns a valid path on the DFS or "" if no valid path can be found.
    */
   std::string StripPrefix(const std::string& file) const;

   /**
    * @brief CleanFilename removes leading and trailing whitespace characters
    * and replaces duplicate forward slashes.
    * @param file is the full path to a file.
    * @return Returns a cleaned file path. For example, if the input is
    * "  ///directory//name ", the function returns "/directory/name".
    */
   std::string CleanFilename(const std::string& file) const;

   /**
    * @brief HostnamesToSlaveIDs associates hostnames, used by HDFS with
    * the slave IDs, used by Mesos.
    * @return Returns an hostname --> slave ID map.
    */
   std::unordered_map<std::string, std::string> HostnamesToSlaveIDs();

   /**
    * @brief BlocksPerHost returns the hostnames of the machines that contain
    * blocks of the specified file along with the number of blocks that each
    * host contains.
    * @param filename is the full path to the file of interest
    * @param unique_blocks is the number of total unique blocks used by the
    * file. Due to replication, the total number of blocks is a multiple of this
    * number.
    * @return Returns a map of "hostname" --> "number of blocks on host."
    */
   StringIntMap BlocksPerHost(const char* filename, int& unique_blocks);

   hdfsFS fs_;

   // "default" : use default NameNode listed in the Hadoop settings.
   std::string namenode_ = "default";
   std::string mesos_master_ = "";
   std::unordered_map<std::string, std::string> hosts_to_slaves_ ;
};


} // ns nebulos

#endif // DFS_TOOLS_HPP
