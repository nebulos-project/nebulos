/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NEBULOS_HPP
#define NEBULOS_HPP

#include <vector>
#include <string>
#include <iostream>

/// The number of retired tasks whose command info will remain in memory.
/// If the number of retired tasks exceeds this threshold, the information
/// associated with these tasks will be cached to disk. There is a tradeoff
/// here: a large threshold wastes memory and slows the process of searching
/// through the list of tasks when certain operations are performed. On the
/// other hand, a small threshold causes the caching operation to be performed
/// more frequently. 
///
/// \todo Allow the user to specify the threshold.
#define CACHE_THRESHOLD 2500

namespace nebulos {

typedef std::vector<std::string> StringList;
typedef unsigned long long int taskid; // 64-bits
typedef unsigned long long int ullint;
typedef std::vector<taskid> taskids;


/**
 * @brief The BatchSettings struct contains the job settings, needed by Mesos
 * and NebulOS.
 */
struct JobSettings
{
   std::string mode {"batch"}; // only "streaming" actually has meaning
   std::string master;
   std::string role;
   std::string threads_per_task;
   std::string mem_per_task;
   std::string disk_per_task;
   std::string monitor;
   std::string relauncher;
   std::string update_interval;
   std::string output_target;
   std::string log_directory;
   std::string job_id;
   std::string verbosity;
};

// namespaces are used for enumerations, rather than using enum classes
// because enum classes complicated things too much. Namespaces help with
// the clarity of the code so that the enumerations are less mysterious.

namespace Signal {

/**
 * @brief The Signal enum specifies the behavior of the killTask() methods.
 *
 * This enumeration works along with the TaskID that is sent by
 * mesos::SchedulerDriver::killTask() to specify what action should be taken
 * when the executor member function, NebulOSExecutor::killTask() is
 * subsequently called by the mesos::ExecutorDriver.
 */
enum signal {
   KILL,           ///< Terminate the task.
   SEND_STD_INPUT, ///< Send data to the task's standard input stream.
   GET_STD_OUTPUT, ///< Get data from the task's standard output stream.
   BEGIN_PERSIST,  ///< The task should begin persisting, rather than closing.
   EXEC_COMMAND,   ///< In persistence mode, execute a new command.
   END_PERSIST     ///< End persistence mode: close the thread and clean up.
};

} // ns Signal


namespace FieldIndex{

/**
 * @brief The FieldIndex enum specifies the meaning of the fields in the
 * UnknownFields used in the TaskID message that is sent by the killTask()
 * methods.
 */
enum field_index {
   SIG = 0, ///< The index of the signal field in the UnknownFields.
   DATA = 1 ///< The index of the data field in the UnknownFields.
};

} // ns FieldIndex

} // ns nebulos

#endif // NEBULOS_HPP
