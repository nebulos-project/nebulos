/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "shared/dfs_tools.hpp"

#include "hdfs.h"

#include <boost/algorithm/string.hpp>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Options.hpp>

#include <algorithm>
#include <sstream>


using namespace std;

namespace nebulos
{


void DfsTools::Connect()
{
   hdfsBuilder* builder = hdfsNewBuilder();

   hdfsBuilderSetNameNode(builder, namenode_.c_str());

   fs_ = hdfsBuilderConnect(builder);
}


void DfsTools::Disconnect()
{
   hdfsDisconnect(fs_);
}


string DfsTools::StripPrefix(const string& file) const
{
   string cleaned_file = CleanFilename(file);

   if (hdfsExists(fs_, cleaned_file.c_str()) == 0)
   {
      return cleaned_file;
   }
   else if (cleaned_file.length() > 1)
   {
      const uint top_directory = cleaned_file.find("/", 1);

      if (top_directory == string::npos)
      {
         cerr << "The file " + cleaned_file + " does not exist.\n";
         return "";
      }

      string substring = cleaned_file.substr(top_directory, string::npos);

      return StripPrefix(substring);
   }
   else
   {
      return "";
   }
}


SlaveFractionList DfsTools::GetHosts(const string &file)
{
   return GetHosts(StringList(1, file));
}


SlaveFractionList DfsTools::GetSlaves(const string& file)
{
   return GetSlaves(StringList(1, file));
}


SlaveFractionList DfsTools::GetHosts(const StringList &files)
{
   int total_unique_blocks = 0;

   StringIntMap total_blocks_per_host;

   for (const string& file : files)
   {
      int unique_blocks = 0;

      StringIntMap blocks_per_host = BlocksPerHost( StripPrefix(file).c_str(),
                                                    unique_blocks);

      if (unique_blocks < 1) { continue; }

      total_unique_blocks += unique_blocks;

      for (const auto& hb_pair : blocks_per_host)
      {
         if (total_blocks_per_host.count(hb_pair.first))
         {
            total_blocks_per_host[hb_pair.first] += hb_pair.second;
         }
         else
         {
            total_blocks_per_host[hb_pair.first] = hb_pair.second;
         }
      }
   }

   SlaveFractionList fractional_ownership;

   if (total_unique_blocks < 1) { return fractional_ownership; }

   const float itotal_unique_blocks = 1.0 / float(total_unique_blocks);

   for (const auto& hb_pair : total_blocks_per_host)
   {
         const float frac = itotal_unique_blocks * hb_pair.second;

         fractional_ownership.emplace_back(hb_pair.first, frac);
   }

   return fractional_ownership;
}


SlaveFractionList DfsTools::GetSlaves(const StringList &files)
{
   if (hosts_to_slaves_.empty())
   {
      hosts_to_slaves_ = HostnamesToSlaveIDs();
   }

   SlaveFractionList fractional_ownership = GetHosts(files);

   for (auto& host_frac_pair : fractional_ownership)
   {
      if (hosts_to_slaves_.count(host_frac_pair.first))
      {
         host_frac_pair.first = hosts_to_slaves_[host_frac_pair.first];
      }
   }

   return fractional_ownership;
}


string DfsTools::CleanFilename(const string &file) const
{
   string trimmed_filename(file);

   boost::algorithm::erase_all(trimmed_filename, "<[");
   boost::algorithm::erase_all(trimmed_filename, "]>");
   boost::algorithm::trim(trimmed_filename);

   if (trimmed_filename.find("//") != string::npos)
   {
      boost::algorithm::replace_all(trimmed_filename, "//", "/");

      return CleanFilename(trimmed_filename);
   }
   else
   {
      return trimmed_filename;
   }
}


unordered_map<string, string> DfsTools::HostnamesToSlaveIDs()
{
   if (!mesos_master_.size())
   {
      cerr << "The mesos_master was not specified. Use mesos_master(string)\n";
      exit(1);
   }

   curlpp::Cleanup RAII_cleanup;

   stringstream json;

   json << curlpp::options::Url(mesos_master_ + "/registrar(1)/registry");

   string raw_slave_info = json.str();

   if (!raw_slave_info.size())
   {
      cerr << "The mesos_master could not be reached. Perhaps the URL is "
              "incorrect.\n";
      exit(1);
   }

   unordered_map<string, string> host_to_slave;

   // A fragile, but cheap way of getting the slave-host data. This is fragile
   // because it depends upon fine details format of the output generated by
   // by the Mesos master; future versions of the Mesos master may break this
   // function by slightly modifying the output of /registrar(1)/registry

   size_t p = raw_slave_info.find("\"hostname\"");

   // the first hostname is the master; skip it
   p = raw_slave_info.find("\"hostname\"", p + 1);

   while (p != string::npos)
   {
      const size_t p1 = p + 12;
      const size_t hostname_end = raw_slave_info.find(",", p1) - 1;
      const size_t host_length = hostname_end - p1;
      const size_t p2 = hostname_end + 17;
      const size_t slave_end = raw_slave_info.find("}", p1) - 1;
      const size_t slave_length = slave_end - p2;
      const string hostname = raw_slave_info.substr(p1, host_length);
      const string slave_id = raw_slave_info.substr(p2, slave_length);

      host_to_slave[hostname] = slave_id;

      p = raw_slave_info.find("\"hostname\"", slave_end);
   }

   return host_to_slave;
}


long DfsTools::FileSize(const char *filename) const
{
   long int file_size = 0;

   hdfsFileInfo* info = hdfsGetPathInfo(fs_, filename);

   if (info)
   {
      file_size = info->mSize;

      hdfsFreeFileInfo(info, 1);
   }

   return file_size;
}


string DfsTools::GetSlaveID(const string &hostname)
{
   if(hosts_to_slaves_.empty())
   {
      QueryMesos(mesos_master_);
   }

   if (hosts_to_slaves_.count(hostname))
   {
      return hosts_to_slaves_[hostname];
   }

   return "";
}


long DfsTools::FileSize(const StringList& files) const
{
   long total = 0;

   for (const string& file : files)
   {
      total += FileSize(StripPrefix(file).c_str());
   }

   return total;
}


int DfsTools::GetReplicationFactor(const char *filename) const
{
   int rep_factor = 0;

   hdfsFileInfo* info = hdfsGetPathInfo(fs_, filename);

   if (info)
   {
      rep_factor = info->mReplication;

      hdfsFreeFileInfo(info, 1);
   }

   return rep_factor;
}


bool DfsTools::SetReplicationFactor(const char *filename, int factor)
{
   return (hdfsSetReplication(fs_, filename, factor) == 0);
}


long DfsTools::GetBlockSize(const char *filename) const
{
   int blocksize = 0;

   hdfsFileInfo* info = hdfsGetPathInfo(fs_, filename);

   if (info)
   {
      blocksize = info->mBlockSize;

      hdfsFreeFileInfo(info, 1);
   }

   return blocksize;
}


bool DfsTools::CopyToDfs(const string &source, const string &dest)
{
   hdfsBuilder* builder = hdfsNewBuilder();

   hdfsBuilderSetNameNode(builder, NULL);

   hdfsFS source_fs = hdfsBuilderConnect(builder);

   return (hdfsCopy(source_fs, source.c_str(), fs_, dest.c_str()) == 0);
}


bool DfsTools::CopyFromDfs(const string &source, const string &dest)
{
   hdfsBuilder* builder = hdfsNewBuilder();

   hdfsBuilderSetNameNode(builder, NULL);

   hdfsFS dest_fs = hdfsBuilderConnect(builder);

   return (hdfsCopy(fs_, source.c_str(), dest_fs, dest.c_str()) == 0);
}


bool DfsTools::CopyWithinDfs(const string &source, const string &dest)
{
   return (hdfsCopy(fs_, source.c_str(), fs_, dest.c_str()) == 0);
}


bool DfsTools::MoveToDfs(const string &source, const string &dest)
{
   hdfsBuilder* builder = hdfsNewBuilder();

   hdfsBuilderSetNameNode(builder, NULL);

   hdfsFS source_fs = hdfsBuilderConnect(builder);

   return (hdfsMove(source_fs, source.c_str(), fs_, dest.c_str()) == 0);
}

bool DfsTools::MoveFromDfs(const string &source, const string &dest)
{
   hdfsBuilder* builder = hdfsNewBuilder();

   hdfsBuilderSetNameNode(builder, NULL);

   hdfsFS dest_fs = hdfsBuilderConnect(builder);

   return (hdfsCopy(fs_, source.c_str(), dest_fs, dest.c_str()) == 0);
}


bool DfsTools::MoveWithinDfs(const string &source, const string &dest)
{
   return (hdfsRename(fs_, source.c_str(), dest.c_str()) == 0);
}


bool DfsTools::Rename(const string &source, const string &dest)
{
   return (hdfsRename(fs_, source.c_str(), dest.c_str()) == 0);
}


bool DfsTools::Remove(const string &filename)
{
   return (hdfsDelete(fs_, filename.c_str(), 0) == 0);
}


bool DfsTools::RemoveDirectory(const string &dirname)
{
   return (hdfsDelete(fs_, dirname.c_str(), 1) == 0);
}


bool DfsTools::CreateDirectory(const string &dirname)
{
   return (hdfsCreateDirectory(fs_, dirname.c_str()));
}


StringIntMap DfsTools::BlocksPerHost(const char* filename,
                                        int& unique_blocks)
{
   StringIntMap host_to_blocks;

   char*** hosts = hdfsGetHosts(fs_, filename, 0, FileSize(filename));

   if (hosts)
   {
      int i = 0;

      while (hosts[i])
      {
         int j = 0;

         while (hosts[i][j])
         {
            const string& host = hosts[i][j];
            if (host_to_blocks.count(host))
            {
               ++host_to_blocks[host];
            }
            else
            {
               host_to_blocks[host] = 1;
            }
            ++j;
         }
         ++i;
      }
      unique_blocks = i;
   }

   hdfsFreeHosts(hosts);

   return host_to_blocks;
}


} //ns nebulos
