/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UTILITIES_HPP
#define UTILITIES_HPP

#include "shared/nebulos.hpp"


namespace nebulos
{

/**
 * @brief IsCommandString tests whether its argument begins with the magic
 * number, indicating that the string contains commands.
 * @param _command is a semicolon or newline delimited list of commands.
 * @return True, if _command begins with the magic number. Otherwise, returns
 * false.
 */
bool IsCommandString(const std::string& _command);


/**
 * @brief MakeCommandList creates a list of commands from a compound command
 * string.
 * @param _commands is a semicolon or newline delimited list of commands.
 * @return A StringList object with one command per entry.
 */
StringList MakeCommandList(const std::string& _commands);



} // ns nebulos

#endif // UTILITIES_HPP
