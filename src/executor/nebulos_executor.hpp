/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NEBULOS_EXECUTOR_H
#define NEBULOS_EXECUTOR_H

#include "shared/nebulos.hpp"

#include <mesos/executor.hpp>
#include <stout/duration.hpp>
#include <stout/os.hpp>

#include <unordered_map>


namespace nebulos
{


class ProcessContainer;


/**
 * @brief The NebulOSExecutor class manages task execution at a high level.
 *
 * This class is manipulated / driven by the Mesos executor driver. It handles
 * execution of tasks, task status communication, and termination of tasks. Only
 * the high-level details are managed here. Low-level details, such as the
 * actual creation of threads, task monitoring, and output redirection are
 * handled by other classes.
 */
class NebulOSExecutor : public mesos::Executor
{
public:

   virtual ~NebulOSExecutor()
   {
      KillAllTasks();
   }

   virtual void registered(mesos::ExecutorDriver* driver,
                           const mesos::ExecutorInfo& executorInfo,
                           const mesos::FrameworkInfo& frameworkInfo,
                           const mesos::SlaveInfo& slaveInfo)
   {
      std::cerr << "Registered executor on "
                << slaveInfo.hostname() << std::endl;
   }

   virtual void reregistered(mesos::ExecutorDriver* driver,
                             const mesos::SlaveInfo& slaveInfo)
   {
      std::cerr << "Re-registered executor on "
                << slaveInfo.hostname() << std::endl;
   }

   virtual void disconnected(mesos::ExecutorDriver* driver)
   {
      std::clog << "Disconnected from mesos-master.\n";
   }

   virtual void launchTask(mesos::ExecutorDriver* driver,
                           const mesos::TaskInfo& task);

   virtual void killTask(mesos::ExecutorDriver* driver,
                         const mesos::TaskID& taskId);

   virtual void frameworkMessage(mesos::ExecutorDriver* driver,
                                 const std::string& data)
   {
      std::clog << "Received message: " << data << std::endl;
   }

   virtual void shutdown(mesos::ExecutorDriver* driver)
   {
      std::clog << "Shutting down\n";
   }

   virtual void error(mesos::ExecutorDriver* driver,
                      const std::string& message)
   {
      std::cerr << message << std::endl;
   }

private:

   ProcessContainer* GetContainerOf(const std::string& task_id);

   void KillAllTasks();

   std::unordered_map<std::string, ProcessContainer*> taskid_to_container_;
};


} // ns nebulos


#endif // NEBULOS_EXECUTOR_H
