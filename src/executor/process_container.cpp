/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "executor/process_container.hpp"
#include "executor/child_process.hpp"

#include <boost/algorithm/string.hpp>

#include <fstream>

using namespace std;


namespace nebulos
{

void ProcessContainer::ParseSettings(const string& executor_settings)
{
   StringList settings;

   boost::split(settings,
                executor_settings,
                boost::algorithm::is_any_of("|"),
                boost::algorithm::token_compress_off);

   if (settings.size() == 5)
   {
      monitor_ = settings[0];
      relauncher_ = settings[1];
      update_interval_ = stoi(settings[2]);
      output_target_ = settings[3];
      log_directory_ = settings[4];
   }
   else
   {
       cerr << "The executor settings string in task.executor().data() is "
               "not formatted properly:\n";
       cerr << "settings.size() = " << settings.size();
       exit(1);
   }
}

void ProcessContainer::Kill()
{
   const mesos::TaskID& task_id = task_.task_id();

   if(my_task_ && child_)
   {
      child_->Kill();

      // inform Mesos and the scheduler that the task was killed

      mesos::TaskStatus status;
      status.mutable_task_id()->MergeFrom(task_.task_id());
      status.set_state(mesos::TASK_KILLED);
      status.set_data("killed by scheduler.");
      driver_->sendStatusUpdate(status);

      if (my_task_->joinable()) { my_task_->join(); }

      delete my_task_;
      my_task_ = nullptr;

      clog << "Task_" << task_id.value() + " was killed by the scheduler.\n";
   }
   else
   {
      clog << "Warning: I could not kill Task_" + task_id.value() << endl;
   }
}


void ProcessContainer::MakeResubmissionCommand(string& _commands)
{
   // a magic number is used in order to identify this as a command.
   const char command_magic[] = {static_cast<char>(200),
                                 static_cast<char>(210),
                                 static_cast<char>(222),
                                 static_cast<char>(234)};

   _commands.insert(0, command_magic, 4);
}


string ProcessContainer::RemoveFilenameBraces(const string &command)
{
   string cleaned(command);

   boost::algorithm::erase_all(cleaned, "<[");
   boost::algorithm::erase_all(cleaned, "]>");

   return cleaned;
}


void ProcessContainer::RunTaskThread()
{
   const string command = RemoveFilenameBraces(task_.data());

   clog << "Starting NebulOS:" << task_.name() << " with command: "
        << command << endl;

   ParseSettings(task_.executor().data());

   mesos::TaskStatus status;
   status.mutable_task_id()->MergeFrom(task_.task_id());
   status.set_state(mesos::TASK_RUNNING);

   driver_->sendStatusUpdate(status);

   const bool capture_output = (output_target_ == "none"
                                && !monitor_.size()) ? false : true;

   ChildProcess subprocess(command,
                           monitor_,
                           update_interval_,
                           capture_output,
                           capture_output,
                           this);

   if (capture_output) { clog << subprocess.stderr(); } // for debugging

   if (output_target_ == "scheduler" && !subprocess.relaunch())
   {
      status.set_data(subprocess.stdout());
   }
   else if (output_target_ == "disk" && !subprocess.relaunch())
   {
      ofstream outputfile;
      string outfilename = log_directory_ + "/" + task_.name();
      outputfile.open(outfilename.c_str());
      if (outputfile.is_open())
      {
         outputfile << "Command: " << task_.data() << endl;
         outputfile << subprocess.stdout();
         outputfile.close();
      }
   }
   else if (output_target_ == "mesos" && !subprocess.relaunch())
   {
      cerr << subprocess.stderr();
      cout << subprocess.stdout();
   }
   else if (output_target_ == "none" && !subprocess.relaunch())
   {
       // do nothing
   }
   else if (!subprocess.relaunch())
   {
      cerr << "The specified output target (" << output_target_ << ") "
           << " is invalid";

      status.set_state(mesos::TASK_FAILED);
      driver_->sendStatusUpdate(status);
   }

   if (subprocess.relaunch())
   {
      ChildProcess relaunch_handler(relauncher_ + " "
                                    + subprocess.relaunch_args());

      string new_commands = relaunch_handler.stdout();

      MakeResubmissionCommand(new_commands);

      status.set_data(new_commands);

      status.set_state(mesos::TASK_KILLED);
      driver_->sendStatusUpdate(status);
   }
   else
   {
      clog << "Finishing task " << task_.task_id().value() << endl;

      status.set_state(mesos::TASK_FINISHED);
      driver_->sendStatusUpdate(status);
   }
}


} // ns nebulos
