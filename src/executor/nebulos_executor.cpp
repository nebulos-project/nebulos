/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "executor/nebulos_executor.hpp"
#include "executor/process_container.hpp"


using namespace std;

namespace nebulos
{


void NebulOSExecutor::launchTask(mesos::ExecutorDriver* driver,
                                 const mesos::TaskInfo& task)
{
   const string task_id = task.task_id().value();

   taskid_to_container_[task_id] = new ProcessContainer(driver, task);
}


void NebulOSExecutor::killTask(mesos::ExecutorDriver* driver,
                               const mesos::TaskID& taskId)
{
   // convert the integer in the custom SIG field of the taskID to
   // a Signal::signal enumeration member.

   const int& sig_ = taskId.unknown_fields()
                     .field(FieldIndex::SIG)
                     .fixed32();

   const Signal::signal sig = static_cast<Signal::signal>(sig_);

   // if taskID contains a DATA field, store it in a string

   const string& data = (taskId.unknown_fields().field_count() > 1)
                       ? taskId.unknown_fields()
                         .field(FieldIndex::DATA)
                         .length_delimited()
                       : "";

   ProcessContainer* task_container = GetContainerOf(taskId.value());

   if (!task_container) { return; }

   switch (sig)
   {
   case Signal::KILL:           task_container->Kill(); break;
   case Signal::SEND_STD_INPUT: cerr << "do something with data\n"; break;
   case Signal::GET_STD_OUTPUT: cerr << "do something with data\n"; break;
   case Signal::BEGIN_PERSIST:  cerr << "start persisting here\n";  break;
   case Signal::EXEC_COMMAND:   cerr << "do something with data\n"; break;
   case Signal::END_PERSIST:    cerr << "stop persisting\n";        break;
   }

   // for temporary debugging purposes:
   cout << "The data sent with this signal:" << data << endl;

}


void NebulOSExecutor::KillAllTasks()
{
   for (auto& entry : taskid_to_container_)
   {
      entry.second->Kill();
   }
}


ProcessContainer* NebulOSExecutor::GetContainerOf(const std::string& task_id)
{
   if (taskid_to_container_.count(task_id))
   {
      return taskid_to_container_[task_id];
   }

   cerr << "Error in GetContainerOf (task_id): task_id," << task_id
        << " , does not exist.";

   return nullptr;
}


} //ns nebulos



int main(int argc, char** argv)
{
   nebulos::NebulOSExecutor executor;
   mesos::MesosExecutorDriver driver(&executor);
   return ( driver.run() == mesos::DRIVER_STOPPED ) ? 0 : 1;
}


