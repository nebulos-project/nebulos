/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "executor/child_process.hpp"
#include "executor/process_container.hpp"

#include <boost/algorithm/string.hpp>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>


using namespace std;


namespace nebulos{

ChildProcess::ChildProcess(const string& command,
                           const bool capture_output_stream,
                           const bool capture_error_stream,
                           ProcessContainer* parent):
   capture_output_stream_(capture_output_stream),
   capture_error_stream_(capture_error_stream),
   parent_(parent)
{
   init(command);
}


ChildProcess::ChildProcess(const char* program,
                           char* const* arg_list,
                           const bool capture_output_stream,
                           const bool capture_error_stream,
                           ProcessContainer* parent):
   capture_output_stream_(capture_output_stream),
   capture_error_stream_(capture_error_stream),
   parent_(parent)
{
   init(program, arg_list);
}


ChildProcess::ChildProcess(const string& command,
                           const string& monitor,
                           const int interval,
                           const bool capture_output_stream,
                           const bool capture_error_stream,
                           ProcessContainer* parent):
   monitor_(monitor),
   update_interval_(interval),
   capture_output_stream_(capture_output_stream),
   capture_error_stream_(capture_error_stream),
   parent_(parent)
{
   init(command);
}


ChildProcess::ChildProcess(const char *program,
                           char * const *arg_list,
                           const char *monitor,
                           const int interval,
                           const bool capture_output_stream,
                           const bool capture_error_stream,
                           ProcessContainer* parent):
   monitor_(monitor),
   update_interval_(interval),
   capture_output_stream_(capture_output_stream),
   capture_error_stream_(capture_error_stream),
   parent_(parent)
{
   init(program, arg_list);
}


bool ChildProcess::IsRunning()
{
   struct stat statbuffer;

   const string kstatfile_ = "/proc/" + to_string(pid_);

   return (stat(kstatfile_.c_str(), &statbuffer) == 0) && !finished_;
}


void ChildProcess::Wait()
{
   if (!finished_)
   {
      int status;

      int pid = wait (&status);

      if (pid != -1)
      {
         exit_status_ = status;
      }

       finished_ = true;
   }
}


void ChildProcess::Kill()
{
   if (IsRunning() && !killed_)
   {
      kill(pid_, SIGKILL);
      killed_ = true;
      Wait();
   }
}


int ChildProcess::Spawn(const char* program,
                        char* const* arg_list,
                        int* stdout_pipe,
                        int* stderr_pipe)
{
   pid_t child_pid;

   child_pid = fork();

   if (child_pid < 0)
   {
      cerr << "Unable to fork(). errno = " << errno << endl;
      exit(errno);
   }
   else if (child_pid > 0)
   {
      return child_pid;
   }
   else if (child_pid == 0)
   {
      if (stdout_pipe)
      {
         close(stdout_pipe[0]);                // close the read end of the pipe
         dup2(stdout_pipe[1], STDOUT_FILENO);  // connect stdout to write end
      }

      if (stderr_pipe)
      {
         close(stderr_pipe[0]);
         dup2(stderr_pipe[1], STDERR_FILENO);
      }

      execvp(program, arg_list);
      cerr << "An error occurred in execvp: errno = " << errno << endl;
      exit(errno);
   }

   // this point is never reached; this line simply silences a compiler warning.
   return 0;
}

int ChildProcess::Spawn(const std::string& command,
                        int* stdout_pipe,
                        int* stderr_pipe)
{
   string trimmed_command(command);

   boost::algorithm::trim(trimmed_command);

   nebulos::StringList argumentList;

   boost::split(argumentList,
                trimmed_command,
                boost::algorithm::is_any_of(" \t"),
                boost::algorithm::token_compress_on);

   uint nArguments = argumentList.size();

   char** arg_list = new char*[nArguments + 1];

   for (uint i = 0; i < nArguments; ++i)
   {
      arg_list[i] = const_cast<char*>( argumentList.at(i).c_str() );
   }

   arg_list[nArguments] = nullptr;

   pid_t result = ChildProcess::Spawn(arg_list[0],
                                      arg_list,
                                      stdout_pipe,
                                      stderr_pipe);

   delete arg_list;

   return result;
}


string ChildProcess::GetOutput(int *unix_pipe)
{
   FILE* stream = fdopen(unix_pipe[0], "r");

   char child_output[1024];

   string output;

   /** \todo enforce a maximum size / length for the output. This can be done
        using ftell(stream). The maximum value can be a core
        configuration option. A reasonable value may be ~100K. */
   while ( !feof(stream)
           && !ferror(stream)
           && fgets(child_output, sizeof (child_output), stream) != NULL)
   {
      output += child_output;
   }

   if (stream) { fclose(stream); }

   return output;
}


string ChildProcess::GetPartialOutput(int *unix_pipe)
{
   int fd_flags = fcntl(unix_pipe[0], F_GETFL, NULL);

   fcntl(unix_pipe[0], F_SETFL, O_NONBLOCK); // make the unix pipe non-blocking

   FILE* stream = fdopen(unix_pipe[0], "r");

   char child_output[256];

   string output;

   while ( !feof(stream)
           && !ferror(stream)
           && fgets(child_output, sizeof (child_output), stream) != NULL)
   {
      output += child_output;
   }

   if (stream) { fclose(stream); }

   fcntl(unix_pipe[0], F_SETFL, fd_flags); // restore initial state

   return output;
}


bool ChildProcess::MonitorProgress()
{
   const string resub_keyword = "nebulos_resubmit\n";

   const string monitoring_command = monitor_ + " " + to_string(pid_);

   while (IsRunning())
   {
      sleep(update_interval_);

      string partial_stdout, partial_stderr;

      if (stdout_pipe_)
      {
         partial_stdout = GetPartialOutput(stdout_pipe_);
         stdout_ += partial_stdout;
      }

      if (stderr_pipe_)
      {
         partial_stderr = GetPartialOutput(stderr_pipe_);
         stderr_ += partial_stderr;
      }

      ChildProcess* monitor = new ChildProcess (monitoring_command
                                                   + " <STD_OUTPUT>"
                                                   + partial_stdout
                                                   + "</STD_OUTPUT>"
                                                   + "<STD_ERROR>"
                                                   + partial_stderr
                                                   + "</STD_ERROR>");

      const string stdout = monitor->stdout();

      delete monitor;

      if (stdout.size() >= resub_keyword.size())
      {
         if (stdout.compare(0, resub_keyword.size(), resub_keyword) == 0)
         {
            Kill();

            relaunch_args_ = stdout;

            relaunch_args_.erase(0, resub_keyword.size());

            return true;
         }
      }
   }

   return false;
}


void ChildProcess::init(const char* program, char* const *arg_list)
{
   if (parent_) // parent_ is only non-null when a parent ProcessContainer has
   {            // been specified in the constructor.
      parent_->child(this);
   }

   if (capture_output_stream_)
   {
      stdout_pipe_ = new int[2];
      if (pipe(stdout_pipe_) == -1)
      {
         cerr << "pipe() failed. errno = " << errno << endl;
         exit (errno);
      }
   }
   else { stdout_pipe_ = nullptr; }

   if (capture_error_stream_)
   {
      stderr_pipe_ = new int[2];
      if (pipe(stderr_pipe_) == -1)
      {
         cerr << "pipe() failed. errno = " << errno << endl;
         exit (errno);
      }
   }
   else { stderr_pipe_ = nullptr; }

   pid_ = ChildProcess::Spawn(program, arg_list, stdout_pipe_, stderr_pipe_);

   // close the write end of each pipe
   if (stdout_pipe_) { close(stdout_pipe_[1]); }
   if (stderr_pipe_) { close(stderr_pipe_[1]); }

   if (!monitor_.empty()) { relaunch_ = MonitorProgress(); }

   if (!stdout_pipe_ && !stderr_pipe_) { Wait(); }

   if (stdout_pipe_) { stdout_ += GetOutput(stdout_pipe_); }
   if (stderr_pipe_) { stderr_ += GetOutput(stderr_pipe_); }
}


void ChildProcess::init(const string& command)
{
   string trimmed_command(command);

   boost::algorithm::trim(trimmed_command);

   vector<string> argumentList;

   boost::split(argumentList,
                trimmed_command,
                boost::algorithm::is_any_of(" \t"),
                boost::algorithm::token_compress_on);

   uint nArguments = argumentList.size();

   char** arg_list = new char*[nArguments + 1];

   for (uint i = 0; i < nArguments; ++i)
   {
      arg_list[i] = const_cast<char*>( argumentList.at(i).c_str() );
   }

   arg_list[nArguments] = nullptr;

   init(arg_list[0], arg_list);

   delete arg_list;
}

} //ns
