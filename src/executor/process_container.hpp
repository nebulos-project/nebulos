/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PROCESS_CONTAINER_HPP
#define PROCESS_CONTAINER_HPP

#include "shared/nebulos.hpp"

#include <mesos/executor.hpp>

#include <thread>


namespace nebulos
{

class ChildProcess;

/**
 * @brief The ProcessContainer class creates and manages a thread that is used
 * to launch a new child process / task.
 *
 * This class manages a thread that creates a ChildProcess object, which manages
 * a task. One ProcessContainer is created for each simultaneous task launched
 * by the executor.
 */
class ProcessContainer
{
public:

   ProcessContainer(mesos::ExecutorDriver* driver,
                         const mesos::TaskInfo& task):
      driver_(driver),
      task_(task)
   {
      my_task_ = new std::thread(&ProcessContainer::RunTaskThread, this);
   }

   ~ProcessContainer()
   {
      if (my_task_) { delete my_task_; }
   }

   /**
    * @brief accessor function for the ChildProcess pointer.
    * @return Returns a pointer to the ChildProcess managed by this container.
    */
   ChildProcess* child() { return child_; }

   /**
    * @brief settor function for ChildProcess pointer.
    * @param child is a pointer to the ChildProcess object managed by this
    * container.
    */
   void child(ChildProcess* child) { child_ = child; }

   /**
    * @brief Kill terminates the process managed by this container.
    */
   void Kill();

private:

   void ParseSettings(const std::string& executor_settings);

   void MakeResubmissionCommand(std::string& _commands);

   std::string RemoveFilenameBraces(const std::string& command);

   void RunTaskThread();

   mesos::ExecutorDriver* driver_;
   const mesos::TaskInfo task_;
   std::thread* my_task_ {nullptr};
   std::string monitor_;
   std::string relauncher_;
   std::string output_target_;
   std::string log_directory_;
   std::string job_id_;
   uint update_interval_ {0};
   ChildProcess* child_ {nullptr};
};


} // ns nebulos

#endif // PROCESS_CONTAINER_HPP
