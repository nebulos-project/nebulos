/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CHILD_PROCESS_H
#define CHILD_PROCESS_H

#include "shared/nebulos.hpp"


namespace nebulos{

class ProcessContainer;

/**
 * @brief The ChildProcess class spawns and manages a child process.
 *
 * This class spawns a child process and redirects the output of the task using
 * Unix pipes (FIFO buffer). The content of the child process' output can,
 * optionally, be passed to a task monitor, which can instruct this class to
 * kill the process. This is the basis for the NebulOS task monitoring and
 * relaunching system.
 */
class ChildProcess
{
public:

   ChildProcess() {}

   /**
    * @brief ChildProcess constructs the ChildProcess object.
    * @param command is a string containing a command to be launched in a child
    * process.
    * @param capture_output_stream specifies whether the child process's standard
    * output stream should be captured.
    * @param capture_error_stream specifies whether the child process's standard
    * error stream should be captured.
    * @param parent is a pointer to the parent ProcessContainer object that is
    * managing this child process (if one exists).
    */
   explicit ChildProcess(const std::string& command,
                         const bool capture_output_stream = true,
                         const bool capture_error_stream = true,
                         ProcessContainer* parent = nullptr);


   /**
    * @brief ChildProcess constructs the ChildProcess object.
    * @param program is the name of the program being launched
    * @param arg_list is the list of arguments provided to the program.
    * @param capture_output_stream specifies whether the child process's standard
    * output stream should be captured.
    * @param capture_error_stream specifies whether the child process's standard
    * error stream should be captured.
    * @param parent is a pointer to the parent ProcessContainer object that is
    * managing this child process (if one exists).
    */
   explicit ChildProcess(const char* program,
                         char* const* arg_list,
                         const bool capture_output_stream = true,
                         const bool capture_error_stream = true,
                         ProcessContainer* parent = nullptr);


   /**
    * @brief ChildProcess constructs the ChildProcess object.
    * @param command is a string containing a command to be launched in a child
    * process.
    * @param monitor is the task monitoring program that will be used with the
    * child process.
    * @param interval is the interval (in seconds) between calls to the monitor.
    * @param capture_output_stream specifies whether the child process's standard
    * output stream should be captured.
    * @param capture_error_stream specifies whether the child process's standard
    * error stream should be captured.
    * @param parent is a pointer to the parent ProcessContainer object that is
    * managing this child process (if one exists).
    */
   explicit ChildProcess(const std::string& command,
                         const std::string& monitor,
                         const int  interval = 1,
                         const bool capture_output_stream = true,
                         const bool capture_error_stream = true,
                         ProcessContainer* parent = nullptr);


   /**
    * @brief ChildProcess constructs the ChildProcess object.
    * @param program is the name of the program being launched
    * @param arg_list is the list of arguments provided to the program.
    * @param monitor is the task monitoring program that will be used with the
    * child process.
    * @param interval is the interval (in seconds) between calls to the monitor.
    * @param capture_output_stream specifies whether the child process's standard
    * output stream should be captured.
    * @param capture_error_stream specifies whether the child process's standard
    * error stream should be captured.
    * @param parent is a pointer to the parent ProcessContainer object that is
    * managing this child process (if one exists).
    */
   explicit ChildProcess(const char *program,
                         char* const *arg_list,
                         const char* monitor,
                         const int interval = 1,
                         const bool capture_output_stream = true,
                         const bool capture_error_stream = true,
                         ProcessContainer* parent = nullptr);

   ~ChildProcess()
   {
      Kill();

      if (stdout_pipe_) { delete stdout_pipe_; }
      if (stderr_pipe_) { delete stderr_pipe_; }
   }

   ChildProcess(const ChildProcess& other) = delete;
   ChildProcess(ChildProcess&& other) = delete;
   ChildProcess& operator=(const ChildProcess& other) = delete;
   ChildProcess& operator=(ChildProcess&& other) = delete;

   void Run(const std::string& command,
            const bool capture_output_stream = true,
            const bool capture_error_stream = true);

   void Monitor(const std::string& monitor, const int interval = 1)
   {
      monitor_ = monitor;
      update_interval_ = interval;
   }

   /**
    * @brief IsRunning inspects the state of the child process.
    * @return Returns true if the child process is running, false otherwise.
    */
   bool IsRunning();

   /**
    * @brief Wait waits on the child process (i.e., it cleans up).
    */
   void Wait();

   /**
    * @brief Kill kills / terminates the child process.
    */
   void Kill();

   bool IsFinished() const { return finished_; }

   /**
    * @brief pid reports the PID of the child process.
    * @return Returns the process ID number of the child process.
    */
   int pid() const { return pid_; }

   /**
    * @brief exit_status reports the exit status of the child process.
    * @return Returns the exit status number / code of the child process.
    */
   int exit_status() const { return exit_status_; }

   /**
    * @brief relaunch specifies whether the task needs to be relaunched.
    * @return True if the task monitor indicated that the task should be
    * relaunched. False otherwise.
    */
   bool relaunch() const { return relaunch_; }

   /**
    * @brief relaunch_args provides arguments to the relaunch script.
    * @return Returns a string containing the output of the task monitor, which
    * can be used by the relaunch script, for guidance.
    */
   std::string relaunch_args() { return relaunch_args_; }

   /**
    * @brief stdout reports the standard output of the child task.
    * @return Returns the standard output of the child task.
    */
   std::string stdout() const { return stdout_; }

   /**
    * @brief stdout reports the standard error output of the child task.
    * @return Returns the standard error output of the child task.
    */
   std::string stderr() const { return stderr_; }

   /**
    * @brief Spawn creates a child process.
    * @param program is the name of the program that will be executed as a child
    * process.
    * @param arg_list contains the full command that will be executed. The
    * zeroth element of the arg_list array is the program name. The final
    * element is a null pointer.
    * @param stdout_pipe is a Unix pipe (an array of two integers that has been
    * passed to the pipe() system call), used for piping the standard output
    * from the child process to the parent process.
    * @param stderr_pipe is a Unix pipe, used for piping the stendard error
    * stream from the child process to the parent process.
    * @return the process ID of the child process that was spawned.
    *
    * Creates a child subprocess by calling the fork() and execvp() system
    * calls. The standard output stream of the child process is redirected to
    * the Unix pipe (unnamed FIFO), unix_pipe for interprocess communication.
    * If no child process could be created, the program will exit. Exit status
    * will yield the error number, errno.
    */
   static int Spawn(const char *program,
                    char * const *arg_list,
                    int *stdout_pipe = nullptr,
                    int *stderr_pipe = nullptr);

   /**
    * @brief Spawn launches a child process
    * @param command is a command in the form that would be recognized by a Unix
    * shell program, such as Bash.
    * @param stdout_pipe is a Unix pipe (an array of two integers that has been
    * passed to the pipe() system call), used for piping the standard output
    * from the child process to the parent process.
    * @param stderr_pipe is a Unix pipe, used for piping the stendard error
    * stream from the child process to the parent process.
    * @return the process ID of the child process.
    */
   static int Spawn(const std::string& command,
                    int* stdout_pipe = nullptr,
                    int* stderr_pipe = nullptr);


private:

   /**
    * @brief GetOutput extracts the content of the unix_pipe stream and stores
    * it as a std::string.
    * @param unix_pipe a Unix pipe, initialized using the pipe() function call.
    * @return Returns the content of the stream of data that flowed through the
    * unix_pipe.
    */
   std::string GetOutput(int* unix_pipe);

   std::string GetPartialOutput(int* unix_pipe);

   /**
    * @brief MonitorProgress runs the task monitor script to examine the child
    * process.
    * @return Returns true of the monitor ran, false otherwise.
    */
   bool MonitorProgress();

   void init(const std::string& command);

   void init(const char *program, char * const *arg_list);

   int pid_ = -100;
   int exit_status_ = -1;
   std::string monitor_;
   int update_interval_ = 1;
   bool capture_output_stream_ = true;
   bool capture_error_stream_ = true;
   bool finished_ = false;
   bool relaunch_ = false;
   std::string relaunch_args_;
   int* stdout_pipe_ = nullptr;
   int* stderr_pipe_ = nullptr;
   std::string stdout_;
   std::string stderr_;
   bool killed_ {false};
   ProcessContainer* parent_ {nullptr};
};


} //ns

#endif // CHILD_PROCESS_H
