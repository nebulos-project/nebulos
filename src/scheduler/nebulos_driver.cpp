/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scheduler/nebulos_driver.hpp"

using namespace std;

namespace nebulos{


void Driver::Init(const JobSettings& settings)
{
   settings_ = settings;

   string executor_filename;

   const char* kexec_path = getenv("NEBULOS_HOME");

   if (kexec_path != nullptr)
   {
      executor_filename = string(kexec_path) + "/sbin/nebulos-exec";
   }
   else
   {
      executor_filename = string(getenv("PWD")) + "/nebulos-exec";
   }

   if (access(executor_filename.c_str(), F_OK) != 0)
   {
      cerr << "The executor, nebulos-exec, cannot be found. Specify its "
              "location with NEBULOS_HOME; It should be in NEBULOS_HOME/etc/\n";
      exit(errno);
   }

   const string monitor = settings_.monitor + '|'
                          + settings_.relauncher + '|'
                          + settings_.update_interval + '|'
                          + settings_.output_target + '|'
                          + settings_.log_directory;

   executor_.mutable_executor_id()->set_value("default");
   executor_.mutable_command()->set_value(executor_filename);
   executor_.set_name("NebulOS Executor");
   executor_.set_data(monitor);

   framework_.set_user(""); // use "" to Have Mesos fill in the current user.
   framework_.set_name("NebulOS:" + settings_.job_id);
   framework_.set_role(settings_.role);
}


StringList Driver::RunBatch(const JobSettings settings, const char *commands)
{
   Init(settings);

   if (scheduler_)
   {
      cerr << "scheduler already exists! Do not call RunBatch after the "
              "scheduler has been created.\n";
      exit(1);
   }

   if (mesos_driver_)
   {
      cerr << "mesos_driver already exists! Do not call RunBatch after the "
              "driver has been created.\n";
      exit(1);
   }

   scheduler_ = new NebulOSScheduler(executor_, settings_, string(commands));

   mesos_driver_ = new mesos::MesosSchedulerDriver(scheduler_,
                                                   framework_,
                                                   settings_.master);

   if ( mesos_driver_->run() != mesos::DRIVER_STOPPED)
   {
      cerr << "Warning: The scheduler driver did not close properly. "
              "It was either manually halted or encountered a fatal error.\n";
   }

   return scheduler_->GetResults();
}


void Driver::Run(const char *commands)
{
   if (scheduler_ == nullptr)
   {
      scheduler_ = new NebulOSScheduler(executor_, settings_, string(commands));
   }

   if (mesos_driver_ == nullptr)
   {
      mesos_driver_ = new mesos::MesosSchedulerDriver(scheduler_,
                                                      framework_,
                                                      settings_.master);
   }

   if ( mesos_driver_->run() != mesos::DRIVER_STOPPED )
   {
      cerr << "Warning: The scheduler driver did not close properly. "
              "It was either manually halted or encountered a fatal error.\n";
   }

   // note: mesos::DRIVER_RUNNING may be useful along with mesos_driver_->join()
}


void Driver::CreateStream(const char *commands)
{
   if (scheduler_ == nullptr)
   {
      scheduler_ = new NebulOSScheduler(executor_, settings_, string(commands));
   }

   if (mesos_driver_ == nullptr)
   {
      mesos_driver_ = new mesos::MesosSchedulerDriver(scheduler_,
                                                      framework_,
                                                      settings_.master);
   }

   mesos_driver_->start();
}


void Driver::AddToStream(const char *commands)
{
   StringList new_commands = MakeCommandList(commands);

   if (mesos_driver_->reviveOffers() == mesos::DRIVER_RUNNING)
   {
      scheduler_->AddTasks(new_commands);
   }
   else
   {
      std::cerr << "Warning: Tasks were not added because the mesos_driver "
                   "is not running. Did you try to add tasks to a stream after "
                   "the stream was already closed? [i.e., did you wait() on the"
                   "stream already?]\n";
   }
}


bool Driver::Wait()
{
   if (mesos_driver_ == nullptr)
   {
      cerr << "Wait() is meaningless until the mesos driver has started.\n";
      return false;
   }

   if (scheduler_ == nullptr)
   {
      cerr << "The scheduler does not exist.\n";
      return false;
   }

   scheduler_->Wait();

   const bool success = (mesos_driver_->join() == mesos::DRIVER_STOPPED);

   return success;
}


} //ns nebulos
