/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BASIC_TASK_MANAGER_H
#define BASIC_TASK_MANAGER_H

#include "scheduler/abstract_task_manager.hpp"
#include "shared/utilities.hpp"

#include <unordered_map>

namespace nebulos
{


class NebulOSScheduler;

/**
 * @brief The BasicTaskManager class manages the tasks assigned to the scheduler.
 * This scheduler is best-suited for cases in which there is little or no input
 * data, other than command parameters, since it ignores the placement of data
 * on the Hadoop Distributed File System.
 *
 * This BasicTaskManager launches tasks in the same order in which they are fed
 * to the scheduler, with the exception that lost tasks will be re-launched
 * as soon as they are lost (i.e., before all tasks have been submitted). The
 * manager tracks the status of each command that is submitted. It also chooses
 * which tasks will be launched next. Lost tasks are automatically re-launched
 * up to three times. The class also allows new tasks to be added at any time.
 */
class BasicTaskManager : public AbstractTaskManager
{
public:

   /**
    * @brief BasicTaskManager constructor accepting a StringList of commands.
    * @param parent is a pointer to the NebulOS scheduler that is using this
    * task manager.
    * @param commands is a StringList of commands, with one command per entry.
    */
   BasicTaskManager(const NebulOSScheduler* parent, const StringList& commands)
      : parent_(parent)
   {
      taskid id = 1;

      for (const std::string& str : commands)
      {
         tasks_.push_back({id, str, ktask_untouched_});

         id_index_[id] = id - 1;

         ++id;
      }

      total_unique_tasks_ = tasks_.size();

      InitCachefile();
   }

   /**
    * @brief BasicTaskManager constructor accepting a string of commands.
    * @param parent is a pointer to the NebulOS scheduler that is using this
    * task manager.
    * @param commands is a string containing newline or semicolon-delimited
    * commands.
    */
   BasicTaskManager(const NebulOSScheduler *parent, const std::string& commands)
      : parent_(parent)
   {
      StringList command_list = MakeCommandList(commands);

      taskid id = 1;

      for (const std::string& str : command_list)
      {
         tasks_.push_back({id, str, ktask_untouched_});

         id_index_[id] = id - 1;

         ++id;
      }

      total_unique_tasks_ = tasks_.size();

      InitCachefile();
   }

   void Activate() override {}

   mesos::TaskInfo NextTask(const mesos::Offer& offer) override;

   bool JobComplete() const override
   {
      return (TasksRemaining() == 0);
   }

   bool SubmissionComplete() const override
   {
      return (total_unique_tasks_ == unique_tasks_submitted_
              && lost_tasks_.size() == 0);
   }

   void AddTasks(const StringList& commands) override;

   void AddTask(const std::string& command) override;

   bool Retire(const taskid task_id) override;

   void TaskLost(const taskid task_id) override;

   void SlaveLost(const std::string& slave) override;

   void ClearSlave(const std::string& slave) override;

   virtual ullint TasksRemaining() const override
   {
      return total_unique_tasks_ - completed_;
   }

   virtual ullint TasksCompleted() const override
   {
      return completed_;
   }

   ullint TasksLost() const override;

   uint TasksRunning() const override;

   ullint TotalTasks() const override
   {
      return total_unique_tasks_;
   }

   taskids LostTasks() const override;

   taskids RunningTasks() const override;

   taskids CompletedTasks() const override;

   taskids RemainingTasks() const override;

   std::string Command(const taskid task_id) const override;

   std::string Status(const taskid task_id) const override;

   taskids Find(const std::string& pattern) const override;

   void CacheCompletedTasks() override;

protected:

   /**
    * @brief FindNextTask finds the next task that needs to be launched.
    * @return
    */
   Task* FindNextTask();

   /**
    * @brief IndexOf identifies the index of the associated task ID.
    * @param id is the task ID number of the task in question.
    * @return Returns the index of the task in the tasks_ vector.
    */
   ullint IndexOf(const taskid id) const
   {
      if (id_index_.count(id))
      {
         return id_index_.at(id);
      }

      std::cerr << "The task id " << id << " does not exist!\n";

      exit(1);
   }

   /**
    * @brief InitCachefile initializes cachefile_ with a unique filename.
    */
   void InitCachefile();

   // task status states

   static const char ktask_retired_ {1};
   static const char ktask_untouched_ {0};
   static const char ktask_selected_ {-1};
   static const char ktask_relaunched_once_ {-2};
   static const char ktask_relaunched_twice_ {-3};
   static const char ktask_relaunched_thrice_ {-4};

   const NebulOSScheduler* parent_;

   /**
    * @brief tasks_ is a vector of tasks that have been assigned to the
    * scheduler.
    */
   Tasks tasks_;

   /**
    * @brief slave_tasks_ stores the taskids of tasks that are currently running
    * on each slave.
    *
    * This list can be used to determine which tasks were lost when a slave
    * is lost, which is useful for determining whether all lost tasks have been
    * resubmitted.
    */
   std::unordered_map<std::string, taskids> slave_tasks_;

   /**
    * @brief id_index_ maps task id numbers to the corresponding index in the
    * tasks_ vector.
    */
   std::unordered_map<taskid, ullint> id_index_;

   /**
    * @brief lost_tasks_ stores a list of tasks that are currently in the "lost"
    * state.
    */
   taskids lost_tasks_;

   /**
    * @brief a count of the number of tasks that have completed.
    */
   ullint completed_ {0};

   /**
    * @brief a count of the number of tasks that have been submitted.
    *
    * When the value of this counter reaches total_unique_tasks_, then all tasks
    * have been submitted.
    */
   ullint unique_tasks_submitted_ {0};

   /**
    * @brief a count of the total number of unique tasks.
    */
   ullint total_unique_tasks_ {0};

   /**
    * The cachefile_ is the name of a file that is used for storing information
    * about retired tasks.
    */
   std::string cachefile_;
};


} //ns nebulos


#endif // BASIC_TASK_MANAGER_H
