/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
   \todo Enable the Mesos authentication (principal, secret pair). See
   http://mesos.apache.org/blog/framework-authentication-in-apache-mesos-0-15-0/
*/

#ifndef NEBULOS_SCHED_H
#define NEBULOS_SCHED_H

#include "shared/nebulos.hpp"
#include "scheduler/basic_task_manager.hpp"
#include "scheduler/dfs_aware_task_manager.hpp"
#include "scheduler/offer_info.hpp"

#include <mesos/scheduler.hpp>
#include <mesos/resources.hpp>

#include <map>
#include <mutex>


namespace nebulos
{

/**
 * @brief RequestResourcesAny requests resources on any host.
 * @param driver is a pointer to the mesos scheduler driver associated with this
 *  scheduler.
 * @param resources is a mesos::Resources object describing the requested
 * resources.
 */
void RequestResourcesAny(mesos::SchedulerDriver* driver,
                         const mesos::Resources& resources);


/**
 * @brief The NebulOSScheduler class is the scheduler at the core of the
 * framework.
 *
 * The scheduler requests Resource offers from the Mesos master. When resource
 * offers are received, the scheduler decides how to distribute the tasks that
 * is has been assigned and then issues commads to the NebulOSExecutor objects,
 * running on the slave nodes. When slave nodes are lost, this scheduler
 * reassigns tasks to other slave nodes. The scheduler also collects
 * information about the tasks that have been perfoomed. For instance, the
 * standard output stream of each task executable can be collected in the
 * results member.
 */
class NebulOSScheduler : public mesos::Scheduler
{
public:

   /**
     * @brief NebulOSScheduler The only constructor for the scheduler.
     * @param executor is the Mesos ExecutorInfo object, which must be defined
     * before initializing the scheduler.
     * @param settings contains the global job settings.
     */
   NebulOSScheduler(const mesos::ExecutorInfo& executor,
                   const JobSettings settings,
                   const std::string& commands)
      : executor_(executor),
        settings_(settings)
   {
      resources_ = mesos::Resources::parse(
                      "cpus:" + settings_.threads_per_task
                      + ";mem:" + settings_.mem_per_task
                      + ";disk:" + settings_.disk_per_task
                      ).get();

      if (commands.find("<[") == std::string::npos
          || commands.find("]>") == std::string::npos)
      {
         task_manager_ = new BasicTaskManager(this, commands);
      }
      else
      {
         task_manager_ = new DfsAwareTaskManager(this, commands, settings);
      }

      resources_offered_ = new OfferInfo(resources_);
   }

   virtual ~NebulOSScheduler()
   {
      task_manager_mtx_.lock();
      delete task_manager_;
      task_manager_mtx_.unlock();
      delete resources_offered_;
   }

   virtual void registered(mesos::SchedulerDriver* driver,
                           const mesos::FrameworkID& id,
                           const mesos::MasterInfo& masterInfo)
   {
      mesos_master_ = masterInfo.hostname() + ":"
                      + std::to_string(masterInfo.port());

      driver_ = driver;

      // Finish initializing the task manager before receiving resource offers.
      task_manager_mtx_.lock();
      task_manager_->Activate();
      task_manager_mtx_.unlock();
   }

   /**
    * \todo Reregistration needs to be handled. Here are some comments regarding
    * this: http://mesos.apache.org/documentation/latest/reconciliation/
    */
   virtual void reregistered(mesos::SchedulerDriver* driver,
                             const mesos::MasterInfo& masterInfo)
   {
      mesos_master_ = masterInfo.hostname() + ":"
                      + std::to_string(masterInfo.port());

      driver_ = driver;

      std::clog << "Re-registered scheduler with mesos-master on "
                << mesos_master_ << std::endl;
   }

   virtual void disconnected(mesos::SchedulerDriver* driver)
   {
      std::clog << "Disconnected from mesos-master, " << mesos_master_
                << std::endl;
   }

   virtual void resourceOffers(mesos::SchedulerDriver* driver,
                               const std::vector<mesos::Offer>& offers);

   /** \todo Test this situation. It appears that we do not need to do anything.
    * because the TASK_LOST case has already been handled.*/
   virtual void offerRescinded(mesos::SchedulerDriver* driver,
                               const mesos::OfferID& offerId)
   {
      std::cerr << "offer " << offerId.value() << " rescinded.\n";
   }

   virtual void statusUpdate(mesos::SchedulerDriver* driver,
                             const mesos::TaskStatus& status);

   virtual void frameworkMessage(mesos::SchedulerDriver* driver,
                                 const mesos::ExecutorID& executorId,
                                 const mesos::SlaveID& slaveId,
                                 const std::string& data)
   {
      std::clog << "Received message: " << data << std::endl;
   }

   virtual void slaveLost(mesos::SchedulerDriver* driver,
                          const mesos::SlaveID& sid);

   virtual void executorLost(mesos::SchedulerDriver* driver,
                             const mesos::ExecutorID& executorID,
                             const mesos::SlaveID& slaveID,
                             int status)
   {
      std::clog << "Executor " << executorID.value() << " on slave "
                << slaveID.value() << " was lost.\n";

      task_manager_mtx_.lock();
      task_manager_->SlaveLost(slaveID.value());
      task_manager_mtx_.unlock();
   }

   virtual void error(mesos::SchedulerDriver* driver,
                      const std::string& message)
   {
      std::cerr << message << std::endl;
   }

   /**
     * @brief GetResults provides access to the standard output of tasks.
     * @return A vector of strings. The ith string contains the standard output
     * steam of the ith task that completed successfully; the order of the
     * output is not guaranteed to match the order of the input commands list.
     * in fact, it almost never be ordered in the same way. Only the results
     * that arrived since the last call to GetResults() will be returned, so
     * a series of GetResults() invocations will each yield partial results.
     */
   StringList GetResults()
   {
      StringList partial_results;

      results_mtx_.lock();
      partial_results.swap(results_);
      results_mtx_.unlock();

      return partial_results;
   }

   const mesos::ExecutorInfo& executor() const { return executor_; }

   const JobSettings& settings() const { return settings_; }

   std::string mesos_master() const { return mesos_master_; }

   mesos::SchedulerDriver* driver() const { return driver_; }

   void driver(mesos::SchedulerDriver* driver) { driver_ = driver; }

   mesos::Resources resources() const { return resources_; }

   OfferInfo* resources_offered() const { return resources_offered_; }

   /**
    * @brief HostOfTask finds the host associated with the given task ID.
    * @param tid is the task id number of the task in question.
    * @return Returns a pointer to a string containing the name of the host
    * on which the task was assigned. If no host is associated with the task,
    * the function returns a nullptr.
    *
    * HostOfTask identifies the host to which a particular task has been
    * assigned. If the task is running (or the task has completed), this host is
    * the host that the task is running on (or ran on). If the task has not yet
    * been launched, this is the host that it will run on. If a task is in the
    * LOST state, then this returns either the host that the first attempt was
    * made on or the host on which the task will be relaunched, depending on the
    * details of the timing. In general, the host can change until the task has
    * been retired.
    */
   const std::string* HostOfTask(const taskid tid) const
   {
      return host_of_task_.count(tid) ? host_of_task_.at(tid) : nullptr;
   }

   /**
    * @brief Idle indicates whether or not the job is idle.
    * @return Returns true if no tasks submited by this scheduler are running
    * or waiting to run on the cluster. Otherwise, retruns false.
    */
   bool Idle() const { return task_manager_->JobComplete(); }

   /**
    * @brief Halt shuts down the scheduler. All executors and tasks are killed.
    */
   void Halt()
   {
      if (driver_->stop(false) != mesos::DRIVER_STOPPED)
      {
         std::cerr << "The driver did not stop.\n";
      }
   }

   /**
    * @brief KillTask instructs the scheduler to kill / terminate a task.
    * @param tid is the ID number of the task taht will be killed.
    * @return Returns true if the kill message was successfully sent. Returns
    * false if the kill message was not sent.
    *
    * KillTask is used to terminate a specific task. Note that the return value
    * only indicates whether the message was successfully submitted to the
    * scheduler driver; a return value of true does NOT mean that the task was
    * sucessfully killed. To test whether a task was killed, check to see if it
    * is running, using RunningTasks() or Status().
    */
   bool KillTask(nebulos::taskid tid);

   /**
    * @brief Wait blocks the thread until all tasks have completed (i.e., until
    * the job becomes idle). It then shuts down the scheduler and the executors.
    */
   void Wait();

   void AddTasks(const StringList& commands);

   ullint TasksRemaining() const;

   ullint TasksCompleted() const;

   ullint TasksLost() const;

   uint TasksRunning() const;

   ullint TotalTasks() const;

   taskids LostTasks() const;

   taskids RunningTasks() const;

   taskids CompletedTasks() const;

   taskids RemainingTasks() const;

   std::string Command(const taskid command_id) const;

   taskids Find(const std::string& pattern) const;

   std::string Status(const taskid task_id) const;

private:

   mesos::ExecutorInfo executor_;
   JobSettings settings_;
   AbstractTaskManager* task_manager_ {nullptr};
   mesos::SchedulerDriver* driver_ {nullptr};
   StringList results_;
   std::unordered_map<taskid, const std::string*> host_of_task_;
   mesos::Resources resources_;
   std::string mesos_master_;
   OfferInfo* resources_offered_;
   std::mutex task_manager_mtx_;
   std::mutex results_mtx_;
};

} // ns nebulos

#endif // NEBULOS_SCHED_H
