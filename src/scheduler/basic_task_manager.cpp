/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scheduler/basic_task_manager.hpp"
#include "scheduler/nebulos_sched.hpp"
#include "scheduler/disk_cache.hpp"

#include <unistd.h>

using namespace std;

namespace nebulos
{


BasicTaskManager::Task* BasicTaskManager::FindNextTask()
{
   if (lost_tasks_.size())
   {
      const taskid next_task_index = IndexOf(lost_tasks_.back());
      lost_tasks_.pop_back();

      if (tasks_[next_task_index].status != ktask_retired_
          && tasks_[next_task_index].status != ktask_selected_)
      {
         cerr << "Relaunching the lost task, " << tasks_[next_task_index].id
              << endl;

         return &tasks_.data()[next_task_index];
      }
   }

   return &tasks_.data()[IndexOf(++unique_tasks_submitted_)];
}


mesos::TaskInfo BasicTaskManager::NextTask(const mesos::Offer& offer)
{
   Task* current_task = FindNextTask();

   if (current_task == nullptr)
   {
      cerr << "\nThere is either a bug in the BasicTaskManager class,\n"
              "or the object is being used incorrectly; FindNextTask\n"
              "returned NULL. FindNextTask should only return NULL if\n"
              "SubmissionComplete() returns true. However, NextTask() and\n"
              "FindNextTask() should only be called if SubmissionComplete()\n"
              "returns false, hence the contradiction.\n";
      exit(1);
   }

   const bool is_normal_submission = (current_task->status == ktask_untouched_);

   if (is_normal_submission) { current_task->status = ktask_selected_; }

   mesos::TaskInfo task;

   task.set_name(parent_->settings().job_id + "_task_"
                 + to_string(current_task->id));
   task.mutable_task_id()->set_value(to_string(current_task->id));
   task.mutable_slave_id()->MergeFrom(offer.slave_id());
   task.mutable_executor()->MergeFrom(parent_->executor());
   task.set_data(current_task->command);

   // associate this task with the slave on which it will be running.
   // (If the slave is later lost, the status of each task in slave_tasks will
   // be checked to ensure that the tasks were sucessfully relaunched.)

   const string& slave = offer.slave_id().value();

   if (slave_tasks_.count(slave))
   {
      slave_tasks_[slave].push_back(current_task->id);
   }
   else
   {
      slave_tasks_[slave] = taskids(1, current_task->id);
   }

   return task;
}


void BasicTaskManager::AddTasks(const StringList& commands)
{
   for (const string& command : commands)
   {
      if (!command.empty())
      {
         taskid id = total_unique_tasks_ + 1;
         tasks_.push_back({id, command, ktask_untouched_});
         const ullint index = tasks_.size() - 1;
         id_index_[id] = index;

         ++total_unique_tasks_;
      }
   }
}


void BasicTaskManager::AddTask(const string& command)
{
   AddTasks(StringList(1, command));
}


bool BasicTaskManager::Retire(const taskid task_id)
{
   bool is_unique_id = false;

   if (id_index_.count(task_id))
   {
      char& status = tasks_[IndexOf(task_id)].status;

      if (status != ktask_retired_)
      {
         ++completed_;
         status = ktask_retired_;
         is_unique_id = true;
      }
   }

   // remove this task from the lost task list, if it appears in the list.

   if (lost_tasks_.size())
   {
      auto iter = find(lost_tasks_.begin(), lost_tasks_.end(), task_id);
      if (iter != lost_tasks_.end())
      {
         lost_tasks_.erase(iter);
      }
   }

   return is_unique_id;
}


void BasicTaskManager::TaskLost(const taskid task_id)
{
   // task 0 is a special task. It is always lost, by design, so we ignore it.

   if (task_id == 0) { return; }

   lost_tasks_.push_back(task_id);

   const ullint task_index = IndexOf(task_id);

   if (tasks_[task_index].status > ktask_relaunched_thrice_
       && tasks_[task_index].status < ktask_untouched_)
   {
      --tasks_[task_index].status;
   }
   else
   {
      Retire(task_id);

      cerr << "task " << task_id << " failed after three attempts. No more "
              "attempts will be made.\n" << flush;
   }
}


void BasicTaskManager::SlaveLost(const string& slave)
{
   cerr << "\nSlave " << slave << " was lost:\n";

   taskids& tasks = slave_tasks_[slave];

   for (int task_id : tasks)
   {
      const int status = tasks_[IndexOf(task_id)].status;

      string description;

      switch (status) {
      case ktask_retired_:
         description = " was rescheduled and has already finished running.\n";
         break;
      case ktask_relaunched_once_:
         description = " has been rescheduled, but it has not yet finished "
                       "running.\n";
         break;
      case ktask_relaunched_twice_:
         description = " was re-scheduled, but it was lost yet again. It was "
                       "subsequently rescheduled once more and has not yet "
                       "finished running.\n";
         break;
      case ktask_relaunched_thrice_:
         description = " was rescheduled, but it was lost twice again. It was "
                       "subsequently re-scheduled for a third time and is now "
                       "running. This will be the final attempt.\n";
         break;
      default:
         description = " was lost and and it was never rescheduled.\n"
                       "I am attempting to reschedule now.\n";
         lost_tasks_.push_back(task_id); /// \todo make sure that the task is actually launched
         break;
      }

      cerr << "Task_" << task_id << description;
   }

   cerr << endl << flush;
}


void BasicTaskManager::ClearSlave(const string& slave)
{
   /// \todo This should only clear tasks that have actually completed!

   if (slave_tasks_.count(slave))
   {
      slave_tasks_[slave].clear();
   }
}


ullint BasicTaskManager::TasksLost() const
{
   ullint counter = 0;

   for (const Task& task : tasks_)
   {
      if (task.status < ktask_selected_)
      {
         ++counter;
      }
   }

   return counter;
}

uint BasicTaskManager::TasksRunning() const
{
   uint counter = 0;

   for (const Task& task : tasks_)
   {
      if (task.status <= ktask_selected_)
      {
         ++counter;
      }
   }

   return counter;
}


taskids BasicTaskManager::LostTasks() const
{
   taskids lost;

   for (const Task& task : tasks_)
   {
      if (task.status < ktask_selected_)
      {
         lost.push_back(task.id);
      }
   }

   return lost;
}

taskids BasicTaskManager::RunningTasks() const
{
   taskids running;

   for (const Task& task : tasks_)
   {
      if (task.status == ktask_selected_)
      {
         running.push_back(task.id);
      }
   }

   return running;
}

taskids BasicTaskManager::CompletedTasks() const
{
   taskids retired;

   for (const Task& task : tasks_)
   {
      if (task.status == ktask_retired_)
      {
         retired.push_back(task.id);
      }
   }

   return retired;
}

taskids BasicTaskManager::RemainingTasks() const
{
   taskids remaining;

   for (const Task& task : tasks_)
   {
      if (task.status != ktask_retired_)
      {
         remaining.push_back(task.id);
      }
   }

   return remaining;
}

string BasicTaskManager::Command(const taskid task_id) const
{
   if(task_id > total_unique_tasks_ + 1 || task_id == 0)
   {
      return "No such task exists\n";
   }
   else if (total_unique_tasks_ > tasks_.size() // task info has been cached
            && id_index_.count(task_id) == 0)   // this task's info was cached
   {
      if (access(cachefile_.c_str(), F_OK) == -1) // cache file does not exist
      {
         return "The task's data was not successfully cached.\n";
      }

      DiskCache cache(cachefile_);

      string sql = "SELECT COMMAND FROM RETIRED WHERE ID=" + to_string(task_id);

      return cache.ExecuteSQL(sql)[0];
   }

   return tasks_.at( IndexOf(task_id) ).command;
}

string BasicTaskManager::Status(const taskid task_id) const
{
   if(task_id > total_unique_tasks_ + 1)
   {
      return "No such task exists\n";
   }
   else if (id_index_.count(task_id) == 0)
   {
      return "retired";
   }

   const char status = tasks_.at( IndexOf(task_id) ).status;

   string meaning;

   switch (status)
   {
   case ktask_retired_: meaning = "retired"; break;
   case ktask_selected_: meaning = "running"; break;
   case ktask_relaunched_once_: meaning = "relaunched once"; break;
   case ktask_relaunched_twice_: meaning = "relaunched twice"; break;
   case ktask_relaunched_thrice_: meaning = "relaunched thrice"; break;
   case ktask_untouched_: meaning = "not yet launched"; break;
   default: meaning = "null"; break;
   }

   return meaning;
}


taskids BasicTaskManager::Find(const string& pattern) const
{
   taskids matches;

   for (const Task& task : tasks_)
   {
      if (task.command.find(pattern) != string::npos)
      {
         matches.push_back(task.id);
      }
   }

   return matches;
}


void BasicTaskManager::CacheCompletedTasks()
{
   /// \todo Cache more information about each task (e.g., the hostname).

   Tasks active_tasks;

   decltype(id_index_) active_id_index;

   // values is a vector of strings where each string contains a list of 
   // (taskID,command) pairs that will later be used in SQL multi-record
   // INSERT operations. SQLite limits the number of records that can be
   // inserted to 500 per function call. Each string in the values 
   // vector corresponds to one multi-record insertion.

   StringList values(1, " ");

   ullint index = 0;

   uint value_idx = 0;

   uint value_counter = 0;

   for (const auto& task : tasks_)
   {
      if (task.status == ktask_retired_)
      {
         ++value_counter;

         if (value_counter == 499)
         {
            ++value_idx;
            values.push_back(" ");
            value_counter = 0;
         }

         values[value_idx] += "(" + to_string(task.id) + ", '" 
                           + task.command + "'),";
      }
      else
      {
         active_tasks.push_back(task);
         active_id_index[task.id] = index++;
      }
   }

   if (values[0].size())
   {
      DiskCache cache(cachefile_);

      /// \todo try using a string instead of an INT as the primary key, 
      /// since we need to support 64-bit unsigned integers, which are
      /// apparently not supported in SQLite. The performance of using
      /// strings rather than ints needs to be tested. INTs are fine until
      /// someone decides to run a few billion tasks.
      cache.ExecuteSQL("CREATE TABLE IF NOT EXISTS RETIRED("
                       "ID INT PRIMARY KEY NOT NULL,"
                       "COMMAND TEXT NOT NULL)");

      const string sql_query_header = "INSERT INTO RETIRED (ID,COMMAND) VALUES ";

      for (string& vals : values)
      {
         vals.back() = ';'; // statements need to end in a semicolon.
         cache.ExecuteSQL(sql_query_header + vals);
      }
   }

   tasks_.swap(active_tasks);
   id_index_.swap(active_id_index);
}


void BasicTaskManager::InitCachefile()
{
   srand(time(NULL));

   cachefile_ = getenv("PWD") + std::string("/.nebulos")
                + parent_->settings().job_id + "_"
                + std::to_string(rand());
}


} //ns nebulos
