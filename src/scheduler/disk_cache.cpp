/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scheduler/disk_cache.hpp"


using namespace std;

namespace nebulos
{


int DiskCache::Callback(void* data, int argc, char** argv, char** col_name)
{
    StringList* res = reinterpret_cast<StringList*>(data);

    for (int i = 0; i < argc; ++i)
    {
       res->push_back(argv[i]);
    }

    return 0;
}

/// \todo This should be generalized. The return value should be a
/// std::unordered_map<std::string, StringList>. Then the Callback would be
/// modified to append to the results: results[col_name[i]].push_back(argv[i])
StringList DiskCache::ExecuteSQL(const string& sql)
{
   char* error_msg;

   StringList results;

   int rc = sqlite3_exec(database_,
                         sql.c_str(),
                         &DiskCache::Callback,
                         &results,
                         &error_msg);

   if( rc != SQLITE_OK )
   {
      cerr << "SQLite error: " << error_msg << endl << flush;
      sqlite3_free(error_msg);

      return StringList();
   }

   return results;
}


} // ns nebulos
