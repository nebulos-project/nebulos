/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scheduler/nebulos_driver.hpp"

#include <cstdlib>

using namespace mesos;
using namespace std;

int main(int argc, char** argv)
{
   if (argc < 3)
   {
      cout << "usage: $ nebulos host:port command \n";
      exit(1);
   }

   nebulos::JobSettings settings;

   settings.master = argv[1];
   settings.role = "*";
   settings.threads_per_task = "2";
   settings.mem_per_task = "1024";
   settings.disk_per_task = "2048";
   settings.output_target = "scheduler";
   settings.job_id = "cpp_ui";

   nebulos::Driver driver;

   driver.RunBatch(settings, argv[2]);

   return 0;
}
