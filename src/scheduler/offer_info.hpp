/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OFFER_INFO_H
#define OFFER_INFO_H

#include "shared/nebulos.hpp"

#include <mesos/resources.hpp>

#include <unordered_set>

/**
 * @brief The OfferInfo class collects information about the resource offers
 * that have been made.
 *
 * This class keeps track of the history of the resource offers that have been
 * made so that the framework can learn about the environment in which it is
 * running. The unique hosts that have been offered to this framework are
 * tracked and the number of execution slots is computed. This allows the
 * framework to make more informed decisions about resource allocation. For
 * instance, if desired resources have never been offered to this framework,
 * and the frameowort is repeatedly offered resources on undesired hosts, it is
 * likely that the desired hosts will remain busy. On the other hand, if we have
 * only been offered unique hosts (i.e., no cycling has occurred), the desired
 * host my very well be available, so we can wait for the Mesos master to offer
 * the resources.
 */
class OfferInfo
{
public:
   OfferInfo() {}

   OfferInfo(mesos::Resources resources) : resources_(resources) {}

   /**
    * @brief ExamineOffers inspects the properties of the given resource offers.
    * @param offers is a list of resoruce offers, provided by the Mesos-master
    * by way of the scheduler driver.
    * @param settings is the JobSettings object describing the current settings.
    *
    * Every offer provided by the master should be examined using ExamineOffers.
    * When this is done properly, the OfferInfo object is able to quickly learn
    * about the worker nodes (hosts) of the cluster on which the framework is
    * running.
    */
   void ExamineOffers(const std::vector<mesos::Offer>& offers,
                      const nebulos::JobSettings& settings);

   /**
    * @brief KnownHost determines whether the host has been offered previously.
    * @param hostname the name of the machine in question.
    * @return Returns true if the host has been offered to this framework
    * previously (i.e., if the host is known to the framework). Otherwise,
    * returns false.
    */
   const bool KnownHost(const std::string& hostname) const
   {
      return (unique_hosts_.count(hostname) == 1);
   }

   /**
    * @brief TotalResources determines the total number of execution slots that
    * have been made available to this framework.
    * @return Returns an estimate of the total number of slots available to this
    * framework. If the system is homogeneous, the number is exact. If the
    * system is heterogeneous, the estimate may be inexact.
    */
   const uint TotalResources() const
   {
      return slots_per_host_ * unique_hosts_.size();
   }

   /**
    * @brief HostName references a string containing the name of a particular
    * host.
    * @param name is a string containing the name of a host.
    * @return Returns a reference to the hostname string within the internal
    * unique_hosts_ set.
    *
    * This method allows the internal unique_hosts_ set to be used for storing
    * hostnames used throughout the scheduler, so there is no need to store the
    * hostnames in multiple locations. The fact that the set contains only
    * unique hostnames makes this an efficient way to store the names of hosts.
    */
   const std::string* HostName(const std::string& name) const
   {
      return unique_hosts_.count(name) ? &(*unique_hosts_.find(name)) : nullptr;
   }

private:

   mesos::Resources resources_;
   std::unordered_set<std::string> unique_hosts_;
   uint slots_per_host_ {1};
};

#endif // OFFER_INFO_H
