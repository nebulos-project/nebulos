/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DFS_AWARE_TASK_MANAGER_H
#define DFS_AWARE_TASK_MANAGER_H

#include "scheduler/basic_task_manager.hpp"
#include "shared/utilities.hpp"

#include <unordered_map>
#include <queue>

namespace nebulos
{

class NebulOSScheduler;
class DfsTools;


/**
 * \brief Launches tasks on machines that contain appropriate data, in order to
 * minimize network traffic.
 *
 * The DfsAwareScheduler preferentially launches tasks on hosts that contain
 * data used by the task. Tasks are generally not launched in the same order
 * that they were sssigned to the scheduler; the manager opportunisitically
 * assigns tasks to the most-qualified slaves when offers arrive. It also
 * requests resources on specific hosts when appropriate resource offers are not
 * recieved quickly. For optimal performance, the RequestAppropriateHosts()
 * member function should be called immediately after the scheduler has
 * registered with the Mesos Master.
 *
 * \todo Optimize for the case in which a very large file needs to be read.
 * Large files should be more aggressivly optimized, regarding rejection of
 * offers on poor hosts. It's better to wait for a while to get a good offer
 * than to accept a bad offer and then cause network traffic and disk usage on
 * other hosts! The current implementation works best for files that use
 * only one or two blocks ( < 256 MB).
**/
class DfsAwareTaskManager : public BasicTaskManager
{

public:

   DfsAwareTaskManager(const NebulOSScheduler* parent,
                       const StringList& commands,
                       const nebulos::JobSettings settings)
      : BasicTaskManager(parent, commands),
        settings_(settings)
   {
      PopulateTaskLocator();
   }

   DfsAwareTaskManager(const NebulOSScheduler *parent,
                       const std::string& commands,
                       const nebulos::JobSettings settings)
      : BasicTaskManager(parent, commands),
        settings_(settings)
   {
      PopulateTaskLocator();
   }

   ~DfsAwareTaskManager();

   void Activate() override
   {
      RequestAppropriateHosts();
   }

   mesos::TaskInfo NextTask(const mesos::Offer& offer) override;

   void AddTasks(const StringList& commands) override
   {
      BasicTaskManager::AddTasks(commands);

      PopulateTaskLocator();

      RequestAppropriateHosts();
   }

   void AddTask(const std::string& command) override
   {
      AddTasks(StringList(1, command));
   }

   void CacheCompletedTasks() override
   {
      BasicTaskManager::CacheCompletedTasks();

      // clean up all traces of the retired tasks

      TaskLocator locate_task;
      locate_task_.swap(locate_task);

      HostLocator locate_host;
      locate_host_.swap(locate_host);

      CheapestFirst cheapest_task;
      cheapest_task_.swap(cheapest_task);

      PopulateTaskLocator();
   }

   /**
    * @brief RequestResourcesSpecific requests resources on a specific host.
    * @param hostname is the name of the host on which we are requesting
    * resources.
    */
   void RequestResourcesSpecific(mesos::SchedulerDriver* driver,
                                 const mesos::Resources& resources,
                                 const std::string& hostname);

   /**
    * @brief RequestResourcesSpecific requests resources on a specific host.
    * @param hostnames is the list of hosts on which we are requesting
    * resources.
    */
   void RequestResourcesSpecific(mesos::SchedulerDriver *driver,
                                 const mesos::Resources &resources,
                                 const StringList &hostnames);

/**
    * @brief RequestAppropriateHosts requests resources on the most optimal
    * hosts for the unlaunched tasks that are currently assigned to the task
    * manager. By "most optimal," we mean the hosts that contain the largest
    * portion of the data used by each task.
    */
   void RequestAppropriateHosts();


   /**
    * @brief ExtractFilenames extracts HDFS filenames from a command.
    * @param command is a command issued to the scheduler
    * @return Returns a list of filenames that were found in the command string.
    * Filenames are wrapped in <[ ]> braces.
    */
   static StringList ExtractFilenames(const std::string& command);


private:

   Task* FindNextTask(const std::string& hostname);

   /**
    * @brief FindUnselectedTask finds a task that has not yet been selected for
    * launching. If all tasks have already been selected, this returns 0;
    * @return Returns the task_id of a task that has not yet been selected for
    * launching (by NextTask())
    * @sideeffects If the slave was rejected, reject_host_ = true and resources
    * are requested on a host / slave that contains the data of interest.
    */
   Task* FindUnselectedTask();

   /**
    * @brief PopulateTaskLocator populates the locate_task_, locate_host_, and
    * cheapest_task_ data structures.
    *
    * Populates the maps and priority queue that are used for identifying
    * optimal tasks to assign to particular hosts.
    */
   void PopulateTaskLocator();

   /**
    * @brief ResourceLikelyAvailable guesses whether a host will be offered to
    * this framework in a reasonable amount of time, based on previous evidence.
    * @param hostname is the hostname of the machine on which we would like to
    * launch a particular task.
    * @param task_id is the task ID of the task in question.
    * @return Returns true if the host is likely to be offered in a reasonable
    * amount of time (a few milliseconds). Otherwise, returns false.
    */
   bool ResourceLikelyAvailable(const std::string& hostname,
                                const ullint task_id);

   // setting up the TaskLocator and HostLocator data structures

   template <typename T>
   struct HighToLow
   {
      bool operator() (const T& a, const T& b) const
      {
         return (a.priority < b.priority);
      }
   };

   struct TaskPriority
   {
      float priority;
      taskid task_id;
   };

   typedef std::priority_queue<TaskPriority,
   std::vector<TaskPriority>,
   HighToLow<TaskPriority>> LocalFirst;

   /// given a hostname, TaskLocator returns the task with the largest fraction
   /// of data on that host.

   typedef std::unordered_map<std::string, LocalFirst> TaskLocator;

   struct HostPriority
   {
      float priority;
      std::string hostname;
   };

   typedef std::priority_queue<HostPriority,
   std::vector<HostPriority>,
   HighToLow<HostPriority>> LargestFirst;

   /// given a task_id, HostLocator returns the host with the largest fraction
   /// of the required data.

   typedef std::unordered_map<taskid, LargestFirst> HostLocator;

   // setting up the CheapestFirst prioritizer

   struct TaskExpense
   {
      long expense;
      taskid task_id;
   };

   struct LowToHigh
   {
      bool operator() (const TaskExpense& a, const TaskExpense& b) const
      {
         return (a.expense > b.expense);
      }
   };

   /// CheapestFirst returns the least expensive (smallest) unfinished task.

   typedef std::priority_queue<TaskExpense,
   std::vector<TaskExpense>,
   LowToHigh> CheapestFirst;

   JobSettings settings_;
   DfsTools* hdfs_ {nullptr};

   /// see documentation for TaskLocator
   TaskLocator locate_task_;

   /// see documentation for HostLocator
   HostLocator locate_host_;

   /// see documentation for CheapestFirst
   CheapestFirst cheapest_task_;

   /// if true, the current host / resource offer will be rejected.
   bool reject_host_ = false;
};


} //ns nebulos


#endif // DFS_AWARE_TASK_MANAGER_H
