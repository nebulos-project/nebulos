/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DRIVER_HPP
#define DRIVER_HPP

#include "shared/nebulos.hpp"
#include "scheduler/nebulos_sched.hpp"


namespace nebulos {

/**
 * @brief The Driver class manages a NebulOSScheduler + Mesos driver pair.
 *
 * This class provides a container that manages the NebulOS Scheduler and its
 * corresponding Mesos driver so that multiple schedulers & mesos drivers can be
 * managed easily. It also facilitates interactive use of the scheduler.
 */
class Driver
{
public:

   Driver() {}

   Driver(const JobSettings& settings)
   {
      Init(settings);
   }

   Driver(const JobSettings& settings, const char* commands)
   {
      RunBatch(settings, commands);
   }

   ~Driver()
   {
      Halt();
   }

   StringList RunBatch(const JobSettings settings, const char* commands);

   void Run(const char* commands);

   /**
    * @brief KillTask instructs the scheduler to kill / terminate a task.
    *
    * Refer to the documentation for NebulOSScheduler::KillTask fo more
    * details.
    */
   bool KillTask(nebulos::taskid tid)
   {
      if (scheduler_ && mesos_driver_)
      {
         return scheduler_->KillTask(tid);
      }
      else
      {
         std::cerr << "I cannot kill a task that does not exist.";
         return false;
      }
   }

   void CreateStream(const char* commands);

   void AddToStream(const char* commands);

   StringList GetResults() const
   {
      if (scheduler_ && mesos_driver_)
      {
         return scheduler_->GetResults();
      }
      else
      {
         return StringList();
      }
   }

   /**
    * @brief Halt shuts down the framework (executors and scheduler).
    */
   bool Halt()
   {
      bool success = false;

      if (mesos_driver_ && scheduler_)
      {
         std::cerr << "Shutting the framework down.\n";

         scheduler_->Halt();

         success = (mesos_driver_->join() == mesos::DRIVER_STOPPED);

         delete scheduler_;
         scheduler_ = nullptr;

         delete mesos_driver_;
         mesos_driver_ = nullptr;
      }

      return success;
   }

   /**
    * @brief Wait blocks the thread until all tasks have completed (i.e., until
    * the job becomes idle). It then shuts down the scheduler and the executors.
    */
   bool Wait();

   uint TasksRemaining() const { return scheduler_->TasksRemaining(); }

   uint TasksCompleted() const { return scheduler_->TasksCompleted(); }

   uint TasksLost() const { return scheduler_->TasksLost();}

   uint TasksRunning() const { return scheduler_->TasksRunning(); }

   ullint TotalTasks() const { return scheduler_->TotalTasks(); }

   taskids LostTasks() const { return scheduler_->LostTasks(); }

   taskids RunningTasks() const { return scheduler_->RunningTasks(); }

   taskids CompletedTasks() const { return scheduler_->CompletedTasks(); }

   taskids RemainingTasks() const { return scheduler_->RemainingTasks(); }

   const std::string* HostOfTask(const taskid command_id) const
   {
      return scheduler_->HostOfTask(command_id);
   }

   std::string Command(const taskid command_id) const
   {
      return scheduler_->Command(command_id);
   }

   taskids Find(const std::string& pattern) const
   {
      return scheduler_->Find(pattern);
   }

   std::string Status(const taskid task_id) const
   {
      return scheduler_->Status(task_id);
   }

private:

   void Init(const JobSettings& settings);

   mesos::MesosSchedulerDriver* mesos_driver_ {nullptr};
   mesos::ExecutorInfo executor_;
   mesos::FrameworkInfo framework_;
   NebulOSScheduler* scheduler_ {nullptr};
   JobSettings settings_;
};

} // ns nebulos


#endif // DRIVER_HPP
