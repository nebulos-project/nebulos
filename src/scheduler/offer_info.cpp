/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scheduler/offer_info.hpp"

#include <mesos/type_utils.hpp>

#include <algorithm>

void OfferInfo::ExamineOffers(const std::vector<mesos::Offer> &offers, const nebulos::JobSettings& settings)
{
   using mesos::Resources;

   for (const mesos::Offer& offer : offers)
   {
      unique_hosts_.insert(offer.hostname());

      Resources remaining = offer.resources();

      uint slots_per_offer = 0;

      while (remaining.flatten().contains(resources_))
      {
         ++slots_per_offer;

         Option<Resources> resources =
                             remaining.find(resources_.flatten(settings.role));

          remaining -= resources.get();
      }

      slots_per_host_ = std::max(slots_per_offer, slots_per_host_);
   }

}
