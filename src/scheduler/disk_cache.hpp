/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DISK_CACHE_HPP
#define DISK_CACHE_HPP

#include "shared/nebulos.hpp"

#include <sqlite3.h>

namespace nebulos
{

/**
 * @brief Caches retired task info to disk.
 *
 * The DiskCache class provides a means of caching information about old,
 * retired tasks to disk so that the information is no longer occupying system
 * memory. It currently uses an SQLite database to achieve this.
 */
class DiskCache
{
public:

   /**
    * @brief DiskCache constructs a DiskCache object.
    * @param filename is the name of the file in which the database / cache file
    * will be stored.
    */
   DiskCache(const std::string& filename) : filename_(filename)
   {
      int rc = sqlite3_open(filename_.c_str(), &database_);

      if(rc)
      {
         std::cerr << "SQLite error: " << sqlite3_errmsg(database_)
                   << std::endl;
         sqlite3_close(database_);
         exit(1); /// \todo Just disable the cache system if this does not
                  /// succeed. Don't bother exiting here.
      }
   }

   ~DiskCache()
   {
      if (database_) { sqlite3_close(database_); }
   }

   /**
    * @brief ExecuteSQL execute an SQL command on the database / cache file.
    * @param sql is an SQL query.
    * @return Returns the results of the SQL query with one result per entry.
    */
   StringList ExecuteSQL(const std::string& sql);

   /**
    * @brief Callback is the callback required by the SQLite C API. Refer to the
    * SQLite documentation for more details.
    */
   static int Callback(void* data, int argc, char** argv, char** col_name);

protected:

std::string filename_;
sqlite3* database_ {nullptr};

};



} // ns nebulos

#endif // DISK_CACHE_HPP
