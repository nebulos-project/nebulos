/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ABSTRACT_TASK_MANAGER_H
#define ABSTRACT_TASK_MANAGER_H

#include "shared/nebulos.hpp"

#include <mesos/scheduler.hpp>
#include <mesos/resources.hpp>


namespace nebulos
{

/**
 * @brief The abstract base class for Task Managers.
 *
 * Task Manager classes derived from this class track the status of each command
 * that is submitted. They also choose which tasks will be launched next. When
 * tasks are lost, a Task Manager instance re-launches them automatically.
 */
class AbstractTaskManager
{
protected:

   struct Task
   {
      taskid id;
      std::string command;
      char status;
   };

   typedef std::vector<Task> Tasks;

public:

   virtual ~AbstractTaskManager() {}

   /**
    * @brief Activate prepares the task manager to be used by completing the
    * initialization process.
    *
    * Typically, the task manager is constructed when the scheduler is
    * constructed. At some later point in time, a MesosSchedulerDriver is
    * created. The driver registers the scheduler with the Mesos Master daemon
    * and calls the scheduler's registered() member function. Before this
    * happens, the task manager is typically in an idle state because it cannot
    * be used yet. Since it is idle, it does not need to be fully initialized
    * yet. Calling Activate() causes any remaining initialization steps to be
    * performed so that the task manager can be used. For instance, the task
    * manager may request resources on specific host machines that it knows will
    * be needed for optimal performance.
    */
   virtual void Activate() = 0;

   /**
    * @brief NextTask finds the next task that is appropriate, given the list
    * of unfinished tasks and the resources being offered.
    * @param offer is a resource offer, provided by the Mesos Master.
    * @return Returns a mesos::TaskInfo object describing the task that will
    * be launched.
    */
   virtual mesos::TaskInfo NextTask(const mesos::Offer& offer) = 0;

   /**
    * @brief JobComplete tests whether all tasks have completed (normally or
    * exceptionally).
    * @return true if all tasks have finished running on the slave nodes, false
    * otherwise. Note: a value of true does not mean that the task completed
    * sucessfully; it only indicates that the task was launched and that a
    * terminal status update was later received. In other words, this returns
    * true only if Retire() has been called for all tasks.
    */
   virtual bool JobComplete() const = 0;

   /**
    * @brief SubmissionComplete tests whether or not all tasks have been
    * submitted via mesos::SchedulerDriver::launchTasks().
    * @return true if all tasks have been submitted, false otherwise.
    */
   virtual bool SubmissionComplete() const = 0;

   /**
    * @brief AddTasks is used to add several new tasks to the scheduling
    * queue at once.
    * @param commands contains a list of commands that will be carried out
    * by the tasks lauched by the scheduler.
    */
   virtual void AddTasks(const StringList& commands) = 0;

   /**
    * @brief AddTask is used to add one new task to the scheduling queue.
    * @param command contains the command that will be executed by the task.
    */
   virtual void AddTask(const std::string& command) = 0;

   /**
    * @brief Retire removes the task with task ID of task_id from the list of
    * tasks that have completed.
    * @param task_id is the task ID of the task, corresponding to
    * mesos::TaskInfo::task_id()
    */
   virtual bool Retire(const taskid task_id) = 0;

   /**
    * @brief TaskLost notifies the task manager that the task with task ID of
    * task_id was lost for some reason. An attempt should be made to re-launch
    * the task (unless the task has repeatedly and consistently been lost).
    * @param task_id is the task ID of the lost task.
    */
   virtual void TaskLost(const taskid task_id) = 0;

   /**
    * @brief SlaveLost notifies the task manager that the indicated slave has
    * failed. This will typically be called long after all of the tasks that
    * were running on that slave have been re-launched. Thus, this function
    * should simply check to make sure that the tasks were successfully re-
    * launched. Also see ClearSlave.
    * @param slave is the slave ID of the slave that has been lost.
    */
   virtual void SlaveLost(const std::string& slave) = 0;

   /**
    * @brief ClearSlave informs the task manager that all tasks that were
    * previously submitted to the indicated slave have completed. Thus, if the
    * slave subsequently becomes lost, only tasks that were assigned to this
    * slave after ClearSlave was called will be investigated when SlaveLost() is
    * called.
    * @param slave is the slave ID of the slave whose history will be cleared.
    */
   virtual void ClearSlave(const std::string& slave) = 0;

   /**
    * @brief TasksRemaining finds the number of tasks that have been submitted
    * to the scheduler, but have not yet been sent to the executors.
    * @return The number of tasks waiting to be sent to the executors.
    */
   virtual ullint TasksRemaining() const = 0;

   /**
    * @brief TasksCompleted finds the total number of tasks that have been
    * launched and are no longer running.
    * @return returns the number of completed tasks. This includes tasks that
    * were were not able to run sucessfully (i.e., tasks that were lost several
    * times) and tasks that were killed by their associated monitor.
    */
   virtual ullint TasksCompleted() const = 0;

   /**
    * @brief TasksLost finds the number of tasks that are currently in the LOST
    * state.
    * @return The number of lost tasks that have not yet been retired.
    */
   virtual ullint TasksLost() const = 0;

   /**
    * @brief TasksRunning finds the number of tasks that are either running or
    * in the process of being launched (staging).
    * @return The number of tasks that are currently running, plus the number of
    * tasks that have been submitted and are waiting to be launched.
    */
   virtual uint TasksRunning() const = 0;

   /**
    * @brief TotalTasks returns the total number of tasks that have been
    * assigned to this task manager.
    */
   virtual ullint TotalTasks() const = 0;

   /**
    * @brief LostTasks finds the ID numbers of tasks that are currently LOST.
    * @return A list of IDs of lost tasks.
    */
   virtual taskids LostTasks() const = 0;

   /**
    * @brief RunningTasks finds the ID numbers of tasks that are either running
    * or have been submitted to the Mesos-master and are waiting to run
    * (i.e., tasks in the STAGING state).
    * @return
    */
   virtual taskids RunningTasks() const = 0;

   /**
    * @brief CompletedTasks finds the ID numbers of tasks that have been
    * launched and are no longer running.
    * @return A list of task IDs of completed (retired) tasks.
    */
   virtual taskids CompletedTasks() const = 0;

   /**
    * @brief RemainingTasks finds the ID numbers of tasks that have not yet
    * finished running. This includes tasks that are currently running and tasks
    * that have not yet been sent to the executors.
    * @return a list of task IDs of un-launched tasks.
    */
   virtual taskids RemainingTasks() const = 0;

   /**
    * @brief Command finds the full command associated with a particular task ID
    * number.
    * @param command_id is the task ID number of the task of interest.
    * @return the full command that was sent (or will be sent) to the executor.
    */
   virtual std::string Command(const taskid command_id) const = 0;

   /**
    * @brief Status finds the status of the task with ID = task_id
    * @param command_id is the task ID number of the task of interest.
    * @return Returns a description of the status of the task.
    */
   virtual std::string Status(const taskid task_id) const = 0;

   /**
    * @brief Find searches the active task commands for a specified pattern.
    * @param pattern is a simple pattern being matched.
    * @return Returns a list of task IDs whose corresponding commands match
    * the pattern.
    */
   virtual taskids Find(const std::string& pattern) const = 0;

   /**
    * @brief CacheCompletedTasks stores data about the completed tasks to the
    * local disk in order to free the system memory and improve performance.
    */
   virtual void CacheCompletedTasks() = 0;
};


} //ns nebulos
#endif // ABSTRACT_TASK_MANAGER_H
