/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scheduler/dfs_aware_task_manager.hpp"
#include "scheduler/nebulos_sched.hpp"
#include "shared/dfs_tools.hpp"
#include "scheduler/disk_cache.hpp"

#include <cstdio>
#include <unordered_set>

using namespace std;

namespace nebulos
{

DfsAwareTaskManager::~DfsAwareTaskManager()
{
   if (hdfs_) { delete hdfs_; }

   std::remove(cachefile_.c_str());
}

StringList DfsAwareTaskManager::ExtractFilenames(const string& command)
{
   // filenames are surrounded with double braces <[/path/to/file]>

   if (command.find("<[") == string::npos || command.find("]>") == string::npos)
   {
      cerr << "Command does not contain filename braces: " << command << endl;
      return StringList(); // no file path is specified in the string
   }

   // verify that the double braces match and that there are no nested braces.

   for (uint i = 0, depth = 0; i < command.size() - 1; ++i)
   {
      if (command.compare(i, 2, "<[") == 0) { ++depth; }

      if (command.compare(i, 2, "]>") == 0) { --depth; }

      if (depth > 1) // because depth is uint, 0-1 > 1;
      {
         cerr << "Skipping command: " << command << endl;
         return StringList();
      }
   }

   StringList files;

   for (uint i = 0; command.find("<[", i) != string::npos;)
   {
      const uint first_idx = command.find("<[", i) + 2;
      const uint last_idx = command.find("]>", first_idx);

      files.push_back(command.substr(first_idx, last_idx - first_idx));

      i = last_idx;
   }

   return files;
}


void DfsAwareTaskManager::PopulateTaskLocator()
{
   if (hdfs_) { delete hdfs_; }

   hdfs_ = new DfsTools();

   hdfs_->mesos_master(settings_.master);

   for (Task& task : tasks_)
   {
      if (task.status == ktask_retired_) { continue; }

      const StringList files = ExtractFilenames(task.command);

      if (files.empty()) { continue; }

      SlaveFractionList hosts_of_files = hdfs_->GetHosts(files);

      // populate locate_task_ and locate_host_

      for (const auto& host_frac : hosts_of_files)
      {
         const TaskPriority task_priority = { host_frac.second, task.id };

         locate_task_[host_frac.first].push(task_priority);

         const HostPriority host_priority = { host_frac.second, host_frac.first };

         locate_host_[task.id].push(host_priority);
      }

      // populate cheapest_task_

      if (task.status == ktask_untouched_)
      {
         const TaskExpense cheapest = { hdfs_->FileSize(files), task.id };

         cheapest_task_.push(cheapest);
      }
   }
}


void DfsAwareTaskManager::RequestAppropriateHosts()
{
   // unique hosts containing the data needed by the current unlaunched tasks
   unordered_set<string> unique_hosts;

   for (const Task& task: tasks_)
   {
      if (task.status == ktask_untouched_ && locate_host_.count(task.id))
      {
         const string& host = locate_host_[task.id].top().hostname;
         unique_hosts.insert(host);
      }
   }

   StringList hostnames(unique_hosts.begin(), unique_hosts.end());

   RequestResourcesSpecific(parent_->driver(), parent_->resources(), hostnames);
}


DfsAwareTaskManager::Task* DfsAwareTaskManager::FindUnselectedTask()
{
   if (SubmissionComplete() || cheapest_task_.empty()) { return nullptr; }

   while (tasks_[IndexOf(cheapest_task_.top().task_id)].status
          != ktask_untouched_)
   {
      cheapest_task_.pop();

      if (cheapest_task_.empty()) { return nullptr; }
   }

   const taskid task_id = cheapest_task_.top().task_id;
   const taskid task_index = IndexOf(task_id);

   if (cheapest_task_.top().expense < 85e6) // task is inexpensive; launch now!
   {
      cheapest_task_.pop();

      return &tasks_.data()[task_index];
   }
   else
   {
      // request resources on an appropriate host

      if (hdfs_->mesos_master() != parent_->mesos_master())
      {
         hdfs_->mesos_master(parent_->mesos_master());
      }

      string desired_host;

      if (locate_host_.count(task_id))
      {
         desired_host = locate_host_[task_id].top().hostname;

         RequestResourcesSpecific(parent_->driver(),
                                  parent_->resources(),
                                  desired_host);
      }

      // if we are likely to be offered resources on the desired host, reject
      // the current offer, otherwise accept it.

      reject_host_ = ResourceLikelyAvailable(desired_host, task_id);

      return &tasks_.data()[task_index];
   }

   return nullptr;
}


DfsAwareTaskManager::Task*
DfsAwareTaskManager::FindNextTask(const std::string& hostname)
{
   reject_host_ = false;

   // If there are lost tasks, relaunch them instead of launching a new task.
   /// \todo arrange things so that the relaunched task attempts to run on a
   /// host containing the appropriate data.

   if (lost_tasks_.size())
   {
      const taskid next_task_index = IndexOf(lost_tasks_.back());
      lost_tasks_.pop_back();

      if (tasks_[next_task_index].status != ktask_retired_
          && tasks_[next_task_index].status != ktask_selected_)
      {
         cerr << "Relaunching the lost task, " << tasks_[next_task_index].id
              << endl;

         return &tasks_.data()[next_task_index];
      }
   }

   if (!locate_task_.count(hostname))
   {
      // The host is either not an HDFS DataNode or it is a DataNode that does
      // not contain data for any of the tasks that we will be performing.
      // In either case, all of the data read by the task will have to be
      // transferred over the network. This is potentially expensive, so
      // We try to find the best unselected task for the host (i.e., the task
      // that uses the least data). If the smallest unselected task uses a large
      // amount of data, we reject the offer and try to request resources on an
      // appropriate host / slave. Finally, if no appropriate host is available,
      // we are forced to transfer the data over the network by accepting the
      // task on a non-ideal host.

      return FindUnselectedTask();
   }

   LocalFirst& best_tasks = locate_task_[hostname];

   // check the task_status_ vector to see if the best task has been
   // launched yet.

   // if the task has not been selected, select it, then remove the task from
   // the best_tasks queue.

   // if the task has already been launched or selected, remove
   // it and check the next task in the queue until a task is found
   // that has not been launched OR there are no tasks remaining in the queue.
   // If an unselected task is found in the queue, select it and remove it from
   // queue. If no more tasks remain in the queue, select one from the list
   // of tasks that have not been launched.

   while (!best_tasks.empty())
   {
      const taskid next_task_index = IndexOf(best_tasks.top().task_id);

      if (tasks_[next_task_index].status == ktask_untouched_)
      {
         best_tasks.pop();

         return &tasks_.data()[next_task_index];
      }
      else
      {
         best_tasks.pop();
      }
   }

   return FindUnselectedTask();
}


mesos::TaskInfo DfsAwareTaskManager::NextTask(const mesos::Offer& offer)
{
   Task* current_task = FindNextTask(offer.hostname());

   if (current_task == nullptr)
   {
      cerr << "\nThere is either a bug in the DfsAwareTaskManager class,\n"
              "or the object is being used incorrectly; FindNextTask\n"
              "returned NULL. FindNextTask should only return NULL if\n"
              "SubmissionComplete() returns true. However, NextTask() and\n"
              "FindNextTask() should only be called if SubmissionComplete()\n"
              "returns false, hence the contradiction.\n";
      exit(1);
   }

   // Submissions that are not resumbissions and are not rejecting an offer
   // are normal submissions.

   const bool is_normal_submission = (current_task->status == ktask_untouched_
                                      && !reject_host_);

   if (is_normal_submission) { current_task->status = ktask_selected_; }

   mesos::TaskInfo task;

   task.mutable_task_id()->set_value(to_string(current_task->id));
   task.mutable_slave_id()->MergeFrom(offer.slave_id());
   task.mutable_executor()->MergeFrom(parent_->executor());

   if (!reject_host_)
   {
      task.set_name(parent_->settings().job_id + "_task_"
                    + to_string(current_task->id));
      task.set_data(current_task->command);
   }
   else // void command is specified, so the offer is rejected.
   {
      task.set_name(parent_->settings().job_id + "_void");
      task.mutable_task_id()->set_value("0");
      task.set_data("0");
      return task;
   }

   // associate this task with the slave on which it will be running.
   // (If the slave is later lost, the status of each task in slave_tasks will
   // be checked to ensure that the tasks were sucessfully relaunched.)

   const string& slave = offer.slave_id().value();

   if (slave_tasks_.count(slave))
   {
      slave_tasks_[slave].push_back(current_task->id);
   }
   else
   {
      slave_tasks_[slave] = taskids(1, current_task->id);
   }

   if (is_normal_submission) { ++unique_tasks_submitted_; }

   return task;
}


void DfsAwareTaskManager::RequestResourcesSpecific(
      mesos::SchedulerDriver* driver,
      const mesos::Resources& resources,
      const string &hostname)
{
   mesos::Request request;

   request.mutable_resources()->MergeFrom(resources);

   request.mutable_slave_id()->set_value(hdfs_->GetSlaveID(hostname));

   vector<mesos::Request> resource_requests(1, request);

   driver->requestResources(resource_requests);
}


void DfsAwareTaskManager::RequestResourcesSpecific(
      mesos::SchedulerDriver* driver,
      const mesos::Resources& resources,
      const StringList &hostnames)
{
   mesos::Request request;

   request.mutable_resources()->MergeFrom(resources);

   vector<mesos::Request> requests(hostnames.size(), request);

   for (uint i = 0; i < hostnames.size(); ++i)
   {
      mesos::Request request;

      const string& host = hostnames[i];

      request.mutable_resources()->MergeFrom(resources);

      request.mutable_slave_id()->set_value(hdfs_->GetSlaveID(host));

      requests[i].mutable_slave_id()->set_value(hdfs_->GetSlaveID(host));
   }

   driver->requestResources(requests);
}


bool DfsAwareTaskManager::ResourceLikelyAvailable(const string& hostname,
                                                  const ullint task_id)
{
   const uint max_attempts = 4 * parent_->resources_offered()->TotalResources();

   static string prev_host = "";
   static taskid prev_task = 0;
   static uint attempts = 0;

   if (prev_task != task_id || prev_host != hostname)
   {
      prev_host = hostname;
      prev_task = task_id;
      attempts = 1;

      return true;
   }
   else
   {
      ++attempts;
   }

   if (parent_->resources_offered()->KnownHost(hostname))
   {
      // we have been offered resources on this slave previously, so it may
      // become available in a reasonable amount of time. Wait for it by
      // cycling through the total number of task slots, up to 20 times before
      // assuming that the resource will not be offered.

      usleep(30000);

      return (attempts < 5 * max_attempts);
   }
   else
   {
      // we have never been offered resources on the desired host, but we also
      // haven't waited for the resource request to be handled. This is where
      // we wait for the mesos master to respond with an offer. If max_attempts
      // is reached, then we assume that the request will not be granted (thus
      // obtaining the resource is not likely, returning false).

      usleep(30000);

      return (attempts < max_attempts);
   }

   return false;
}


} //ns nebulos
