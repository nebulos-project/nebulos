/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scheduler/nebulos_sched.hpp"
#include "shared/utilities.hpp"

#include <algorithm>

#include <unistd.h>

using namespace std;

namespace nebulos
{

typedef std::vector<mesos::TaskInfo> TaskList;


void RequestResourcesAny(mesos::SchedulerDriver* driver,
                         const mesos::Resources &resources)
{
   mesos::Request request;

   request.mutable_resources()->MergeFrom(resources);

   vector<mesos::Request> resource_requests(1, request);

   driver->requestResources(resource_requests);
}


void NebulOSScheduler::slaveLost(mesos::SchedulerDriver *driver,
                                 const mesos::SlaveID &sid)
{
   task_manager_mtx_.lock();
   task_manager_->SlaveLost(sid.value());
   task_manager_mtx_.unlock();
}


void NebulOSScheduler::resourceOffers(mesos::SchedulerDriver* driver,
                                      const vector<mesos::Offer>& offers)
{
   using mesos::Resources;

   resources_offered_->ExamineOffers(offers, settings_);

   //for (size_t i = 0; i < offers.size(); ++i)
   for (const mesos::Offer& offer : offers)
   {
      //const mesos::Offer& offer = offers[i];

      task_manager_mtx_.lock();
      task_manager_->ClearSlave(offer.slave_id().value());
      task_manager_mtx_.unlock();

      std::vector<mesos::OfferID> offer_ids;

      TaskList tasks;

      Resources remaining = offer.resources();

      while (!task_manager_->SubmissionComplete()
             && remaining.flatten().contains(resources_))
      {
         // choose the appropriate next task, based on the offer.

         task_manager_mtx_.lock();
         mesos::TaskInfo task = task_manager_->NextTask(offer);
         task_manager_mtx_.unlock();

         // associate the task with its host. This is only done so that
         // each task's host machine can be identified for debugging purposes.

         taskid tid = stoull(task.task_id().value().c_str());
         host_of_task_[tid] = resources_offered_->HostName(offer.hostname());

         // assign resources to the task and add the task to the list of tasks
         // that will be launched next.

         Option<Resources> resources =
                             remaining.find(resources_.flatten(settings_.role));

         task.mutable_resources()->MergeFrom(resources.get());

         remaining -= resources.get();

         if (task.task_id().value() != "0") { tasks.push_back(task); }
      }

      offer_ids.push_back(offer.id());

      driver->launchTasks(offer_ids, tasks);
   }
}


void NebulOSScheduler::statusUpdate(mesos::SchedulerDriver* driver,
                                   const mesos::TaskStatus& status)
{
   const taskid taskID = stoull(status.task_id().value().c_str());

   if (status.state() == mesos::TASK_LOST)
   {
      task_manager_mtx_.lock();
      task_manager_->TaskLost(taskID);
      task_manager_mtx_.unlock();

      cerr << "Task " << taskID << " was LOST\n";

      if (task_manager_->SubmissionComplete())
      {
         // initial task submission is complete, but a task was lost, so we
         // request more resources to launch it again.

         RequestResourcesAny(driver, resources_);
      }
   }

   if (status.state() == mesos::TASK_KILLED)
   {
      task_manager_mtx_.lock();
      task_manager_->Retire(taskID);
      task_manager_mtx_.unlock();

      if (IsCommandString(status.data()))
      {
         StringList commands = MakeCommandList(status.data());
         task_manager_mtx_.lock();
         task_manager_->AddTasks(commands);
         task_manager_mtx_.unlock();
      }
      else
      {
         cerr << "Task " << taskID << " was KILLED!\n";
      }
   }

   if (status.state() == mesos::TASK_FAILED)
   {
      task_manager_mtx_.lock();
      task_manager_->Retire(taskID);
      task_manager_mtx_.unlock();

      cerr << "Task " << taskID << " FAILED!\n";
   }

   if (status.state() == mesos::TASK_FINISHED)
   {
      task_manager_mtx_.lock();

      // true if this was the first time, false if the task was already retired:
      bool update_is_unique = task_manager_->Retire(taskID);

      // cache old tasks to disk after CACHE_THRESHOLD tasks have completed

      if (task_manager_->TasksCompleted() % CACHE_THRESHOLD == 0)
      {
         task_manager_->CacheCompletedTasks();
      }

      task_manager_mtx_.unlock();

      string id = (settings_.verbosity == "debug") ? to_string(taskID) + ":" : "";

      if (!status.data().empty() && update_is_unique)
      {
         results_mtx_.lock();
         results_.push_back(id + status.data());
         results_mtx_.unlock();
      }
   }

   if (task_manager_->JobComplete() && settings_.mode != "streaming")
   {
      driver->stop();
   }
}


void NebulOSScheduler::AddTasks(const StringList &commands)
{
   task_manager_mtx_.lock();
   task_manager_->AddTasks(commands);
   task_manager_mtx_.unlock();

   driver_->reviveOffers();
}


void NebulOSScheduler::Wait()
{
   while (true)
   {
      if (Idle())
      {
         Halt();
         break;
      }
      else
      {
         /// \todo look for a better way of doing this!
         usleep(500);
      }
   }
}


bool NebulOSScheduler::KillTask(taskid tid)
{
   mesos::TaskID id;
   id.mutable_unknown_fields()->AddFixed32(FieldIndex::SIG, Signal::KILL);
   // note for later:
   //string signal = "kill";
   //id.mutable_unknown_fields()->AddLengthDelimited(0, signal);
   //string received_signal = id.unknown_fields().field(0).length_delimited();
   //cerr << signal << " " << received_signal << endl;
   id.mutable_value()->assign(to_string(tid));
   mesos::Status stat = driver_->killTask(id);

   // return true if the driver is running, because if the driver isn't running,
   // then the task couldn't have been killed. Returning 'true' doesn't
   // imply that the task was killed. It only implies that the driver received
   // the message to kill the task.
   return (stat == mesos::DRIVER_RUNNING);
}


ullint NebulOSScheduler::TasksRemaining() const
{
   return task_manager_->TasksRemaining();
}

ullint NebulOSScheduler::TasksCompleted() const
{
   return task_manager_->TasksCompleted();
}

ullint NebulOSScheduler::TasksLost() const
{
   return task_manager_->TasksLost();
}

uint NebulOSScheduler::TasksRunning() const
{
   return task_manager_->TasksRunning();
}

ullint NebulOSScheduler::TotalTasks() const
{
   return task_manager_->TotalTasks();
}

taskids NebulOSScheduler::LostTasks() const
{
   return task_manager_->LostTasks();
}

taskids NebulOSScheduler::RunningTasks() const
{
   return task_manager_->RunningTasks();
}

taskids NebulOSScheduler::CompletedTasks() const
{
   return task_manager_->CompletedTasks();
}

taskids NebulOSScheduler::RemainingTasks() const
{
   return task_manager_->RemainingTasks();
}

std::string NebulOSScheduler::Command(const taskid command_id) const
{
  return task_manager_->Command(command_id);
}

taskids NebulOSScheduler::Find(const string &pattern) const
{
   return task_manager_->Find(pattern);
}

std::string NebulOSScheduler::Status(const taskid task_id) const
{
   return task_manager_->Status(task_id);
}


} // ns nebulos
