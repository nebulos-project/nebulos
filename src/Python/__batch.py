# NebulOS: a Big Data framework for scientific data analysis.
# Copyright (C) 2015 The Regents of the University of California

# Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
# and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import glob as _glob
import re as _re
import sys as _sys
import os.path as _path
import subprocess as _subprocess
from os import getenv as _env
from __curly_brace_params import parse_glob as _parse_glob
from __nebulos import __run as _run
from __nebulos import __settings as _settings
from __nebulos import __stream as _stream
from __nebulos import __cleanup


class Processor(object):
    def __init__(self,
                 master="",
                 role="*",
                 threads_per_task="2",
                 mem_per_task="1024",
                 disk_per_task="2048",
                 monitor="",
                 relauncher="",
                 update_interval=10,
                 output="scheduler",
                 log_directory="",
                 job_id="untitled",
                 verbosity="quiet",
                 settings=None):
        if settings is None:
            if master == "":
                master = self.__get_mesos_master()
            print("Using Mesos-master at: " + master)
            self.settings = _settings()
            self.settings.mode = str("batch")
            self.settings.master = str(master)
            self.settings.role = str(role)
            self.settings.threads_per_task = str(threads_per_task)
            self.settings.mem_per_task = str(mem_per_task)
            self.settings.disk_per_task = str(disk_per_task)
            self.settings.monitor = str(monitor)
            self.settings.relauncher = str(relauncher)
            self.settings.update_interval = str(update_interval)
            self.settings.output_target = str(output)
            self.settings.log_directory = str(log_directory)
            self.settings.job_id = str(job_id)
            self.settings.verbosity = verbosity
            if output == "disk" and not _path.isdir(log_directory):
                raise IOError(log_directory + " is not a directory.")
        else:
            self.settings = settings

        if self.settings.output_target not in ["scheduler", "disk", "mesos", "none"]:
            raise SyntaxError(self.settings.output_target + " is not a valid output target.")

        self.stream = None

    def explicit_batch(self, commands):
        """
        Performs a batch job, using a list of explicit commands. This is the most general
        and flexible option for launching tasks. Simply create an explicit list of commands
        and feed them to this function.

        :type commands:  Either a string or a list of strings
        :param commands: Either a string of commands separated by newline characters or a
                         list of strings containing individual commands.
        :return: A list of standard output strings---one string per command.
        """
        command_string = str()

        if isinstance(commands, str):
            command_string = commands
        elif isinstance(commands, list):
            command_string = "\n".join(commands)
        else:
            _sys.exit("nebulos.batch(): 'commands' must be a string or a list "
                      "of strings")

        return _run(self.settings, command_string)

    def explicit_stream(self, commands):
        """
        Behaves the same as explicit_batch(), bt does not block the thread. See documentation
        for explicit_batch() for more details.
        """

        command_string = str()

        if isinstance(commands, str):
            command_string = commands
        elif isinstance(commands, list):
            command_string = "\n".join(commands)
        else:
            _sys.exit("nebulos.stream(): 'commands' must be a string or a list "
                      "of strings")

        if self.stream is None or self.stream.closed():
            self.settings.mode = "streaming"
            self.stream = _stream(self.settings)
            self.stream.begin_streaming(command_string)
        else:
            self.stream.add(command_string)

    def wait(self):
        """
        When the processor is operating in streaming mode, wait() causes the processor to
        block the current thread so that the processor behaves as though the stream job
        has been converted into a batch job. This function only returns once all tasks
        have completed.
        :return True if the processor successfully waited for tasks to finish; False if
        no tasks have been submitted.
        """
        if self.stream is None:
            print("Cannot wait on non-existent stream.")
            return False
        else:
            return self.stream.wait()

    def get_results(self):
        """
        Gets the standard output of each task that has completed on the cluster.
        :return a list of strings in which each entry corresponds to the output of a
        single task.
        """
        if self.stream is None:
            return []
        else:
            return self.stream.get_results()

    def count_unfinished_tasks(self):
        """
        Counts the tasks that have not yet completed. This includes the number
        of tasks that are currently running and the tasks that are waiting to be launched.
        """
        if self.stream is None:
            return 0
        else:
            return self.stream.count_unfinished_tasks()

    def count_completed_tasks(self):
        """
        Counts the tasks that have completed. This includes tasks that failed and tasks
        that were killed / terminated.
        """
        if self.stream is None:
            return 0
        else:
            return self.stream.count_completed_tasks()

    def count_lost_tasks(self):
        """
        Counts the tasks that are currently in a LOST state.
        """
        if self.stream is None:
            return 0
        else:
            return self.stream.count_lost_tasks()

    def count_running_tasks(self):
        """
        Counts the tasks that are currently running on the cluster.
        """
        if self.stream is None:
            return 0
        else:
            return self.stream.count_running_tasks()

    def count_total_tasks(self):
        """
        Counts the tasks that have been submitted to the scheduler. This includes tasks
        in all states (finished, running, lost, killed, waiting).
        """
        if self.stream is None:
            return 0
        else:
            return self.stream.count_total_tasks()

    def list_lost_tasks(self):
        """
        Lists tasks that are currently in the LOST state.
        :return A list of task id numbers for the currently lost tasks.
        """
        if self.stream is None:
            return []
        else:
            return self.stream.list_lost_tasks()

    def list_running_tasks(self):
        """
        Lists the tasks that are currently running on the cluster.
        :return A list of task id numbers.
        """
        if self.stream is None:
            return []
        else:
            return self.stream.list_running_tasks()

    def list_completed_tasks(self):
        """
        Lists tasks that have completed. This includes tasks that failed and tasks
        that were killed, in addition to tasks that finished normally.
        :return A list of task id numbers.
        """
        if self.stream is None:
            return []
        else:
            return self.stream.list_completed_tasks()

    def list_unfinished_tasks(self):
        """
        Lists the tasks that have not yet completed. This includes the number
        of tasks that are currently running and the tasks that are waiting to be launched.
        :return A list of task id numbers.
        """
        if self.stream is None:
            return []
        else:
            return self.stream.list_unfinished_tasks()

    def get_command(self, task_id):
        """
        Finds the command associated with a particular task.
        :param task_id: The task id number of the task of interest.
        :return A string containing the command associated with the specified task id.
        """
        if self.stream is None:
            return None
        else:
            return self.stream.get_command(long(task_id))

    def get_status(self, task_id):
        """
        Finds the status of the task with the given task id.
        :param task_id: The task id number of the task of interest.
        :return A string describing the state of the task of interest.
        """
        if self.stream is None:
            return None
        else:
            return self.stream.get_status(long(task_id))

    def get_hostname(self, task_id):
        """
        Finds the host associated with the given task id.
        :param task_id: The task id number of the task of interest.
        :return A string containing the name of the host machine associated with the
        task of interest. If no host is associated with that task or the task does not
        exist, an empty string is returned.
        """
        if self.stream is None:
            return None
        else:
            return self.stream.get_hostname(long(task_id))

    def kill_task(self, task_id):
        """
        Terminates a specific task running on the cluster. Only tasks that are currently
        running can be killed.
        :param task_id: The task id number of the task of interest.
        :return True if the kill signal was successfully sent. False otherwise. Note that
        a return value of True does not mean that the task was successfully terminated.
        Use get_status() to check whether the task was successfully terminated.
        """
        if self.stream is None:
            return None
        else:
            if long(task_id) in self.stream.list_running_tasks():
                return self.stream.kill_task(long(task_id))
            else:
                print(str(task_id) + " is not running, so it cannot be killed.")
                return False

    def search_tasks(self, pattern):
        """
        Searches the commands of the tasks for a particular substring pattern.
        :param pattern: A string to be searched for. Note that regular expressions are
        not supported in this search.
        :return A list of task id numbers corresponding to tasks whose command contains
        the string, pattern.
        """
        if self.stream is None:
            return []
        else:
            return self.stream.search_tasks(str(pattern))

    def halt(self):
        """
        Shuts the framework down and frees all resources. No information about the tasks
        will remain in memory, so this information should be stored prior to invoking halt()
        if the information will be needed later.
        """
        if self.stream is None:
            return False
        else:
            success = self.stream.halt()
            self.stream = None
            return success

    def finished(self):
        """
        Checks whether all tasks submitted to the framework have completed.
        :return True if all tasks have completed, False otherwise.
        """
        if self.stream is None:
            return None
        else:
            return self.stream.count_unfinished_tasks() == 0

    def glob_batch(self, command, pattern):
        """
        Performs a command on a collection of files whose names match a particular pattern.
        This is useful for running the same command on all files in a particular directory.
        The command is parameterized with the filename placeholder %f%. For example,

         glob_batch("gsnap --view gas %f%", "/dfsmount/snapshots/snapshot_20*")

        would run the command glob_batch("gsnap --view gas" on all files in the directory
        /dfsmount/snapshots/ whose file names begin with snapshot_20.

        :param command: A string containing a filename placeholder %f% where the filename
        should be inserted.
        :param pattern: A filename pattern recognized by the standard Python glob utility.
        :return: A list of standard output strings---one string per command (i.e., one for
        each file that matches the pattern).
        """
        commands = self.__make_commands_from_glob(command, pattern)

        if commands is None:
            return

        return self.explicit_batch(commands)

    def glob_stream(self, command, pattern):
        """
        Performs the same task as glob_batch, but does not block. This allows the user to
        check the status of the tasks and add more tasks before the previously-submitted
        tasks have completed. Use wait() to cause the processor to block the current thread
        until all tasks have completed. See documentation for glob_batch for more
        information.
        """
        commands = self.__make_commands_from_glob(command, pattern)

        if commands is None:
            return
        return self.explicit_stream(commands)

    def template_batch(self, command_template, params):
        """
        Performs commands constructed from a template. This is useful for running a set of
        commands which differ in their arguments. For example, if we wanted to run the same
        analysis on several snapshots from various directions, we could do this:

        template_batch("program -direction %c% %c%", params)

        where

           params = ["0.3 1.1 0.67, file1",     OR    params  = [["0.3 1.1 0.67", "file1"],
                     "0.2 0.9 0.7, file1",                       ["0.2 0.9 0.7", "file1"],
                     "0.3 1.1 0.67, file2",                      ["0.3 1.1 0.67", "file2"],
                     "0.5 1.5 0.47, file2"]                      ["0.5 1.5 0.47", "file2"]]

        :param command_template: A string containing the general pattern of the command
        with placeholders, %c%, where the parameters from the params variable will be
        substituted.
        :param params: A list of strings or a list containing lists of strings:

        ["string", "string", "string"]   OR   [["string"], ["string"], ["string"]]

        If the first form is used, commas act as delimiters. Thus,

        ["s1, s2, s3"] is converted to [["s1", "s2", "s3"]]

        and ["s1, s2, s3", "s4, s5, s6"] becomes [["s1", "s2", "s3"], ["s4", "s5", "s6"]]

        :return: A list of standard output strings---one string per command.
        """
        command_list = self.__make_commands_from_template(command_template, params)

        return self.explicit_batch(command_list)

    def template_stream(self, command_template, params):
        """
        Performs the same task as template_batch, but does not block. This allows the user
        to check the status of the tasks and add more tasks before the previously-submitted
        tasks have completed. Use wait() to cause the processor to block the current thread
        until all tasks have completed. See documentation for template_batch for more
        information.
        """
        command_list = self.__make_commands_from_template(command_template, params)

        return self.explicit_stream(command_list)

    def partition_batch(self, partitioner, command, filename):
        # partitioner is a program that splits the input file into files named
        # filename_identifier (it appends somethting, starting with an underscore)
        # The partitioner variable contains the full command line argument needed to
        # run the partitioner, with %f% in place of the filename.
        # command is the command that will be run on the individual partitions,
        # with a %f% in place of the filename
        partitioner.replace("%f%", filename)

        _subprocess.call(partitioner.split())

        # \todo allow for file extensions (strip extension from filename first?)
        return self.glob_batch(command, filename+"_*")

    def __make_commands_from_glob(self, command, pattern):

        if pattern.count("{"):
            return _parse_glob(command, pattern)

        file_list = _glob.glob(pattern)
        if len(file_list) == 0:
            print ("No files matched the specified pattern.")
            return None
        if "%f%" in command:
            return [command.replace("%f%", "<[" + filename + "]>") for filename in file_list]
        elif "%c%" in command:
            return [command.replace("%c%", filename) for filename in file_list]
        else:
            print("The filenames were not used. This might be an error. "
                  "Did you forget the filename placeholder?")
            return command

    def __make_commands_from_template(self, command_template, params):
        if isinstance(params[0], str):
            params = [s.split(",") for s in params]

        n_params = len(params[0])

        for arglist in params:
            if n_params != len(arglist):
                _sys.exit("Each entry in params must have the same number of parameters.")

        nargs = command_template.count("%c%") + command_template.count("%f%")

        if n_params != nargs:
            _sys.exit("The number of template arguments must match the number "
                      "of arguments in the params variable.")

        command_list = list()

        placeholder = _re.compile('(%[c|f]%)')

        param_type = placeholder.findall(command_template)

        for arglist in params:
            command = command_template
            for i, arg in enumerate(arglist):
                if param_type[i] == '%c%':
                    command = command.replace("%c%", str(arg).replace("\n", ""), 1)
                else:
                    command = command.replace("%f%", '<[' + str(arg).replace("\n", "") + ']>', 1)
            command_list.append(command)

        return command_list

    def __get_mesos_master(self):
        nebulos_home = _env("NEBULOS_HOME")
        if nebulos_home:
            master_filename = nebulos_home + "/etc/mesos_master"
            if _path.isfile(master_filename):
                master_file = open(nebulos_home + "/etc/mesos_master")
                master_name = master_file.read()
                if master_name:
                    return master_name.strip()
                else:
                    print("The mesos_master file in " + nebulos_home + " is empty. You need to specify "
                          "the address:port or hostname:port of the Mesos master.")
                    exit()
            else:
                print("The mesos master address file," + master_filename
                      + "does not exist.")
                exit()
        else:
            print("You have not specified the location of the NebulOS home directory. "
                  "Please set the NEBULOS_MASTER environment variable.")
            exit()


    def __exit__(self, type, value, traceback):
        __cleanup()


# \todo Add detailed documentation strings
# \todo Implement a helper function or another batch style for running many commands on a single file
# (so that the filename does not need to be repeated)
#
# example usage:
#
# from nebulos import Processor
#
# gsnapbatch = Processor(master="foam:5050", mem_per_task=512, threads_per_task=2)
#
# res = gsnapbatch.glob_batch("gsnap --view gas -c 0 0 0 %f%", "/dfsmount/snapshots/snapshot_20*")
#
# res = gsnapbatch.glob_batch("gsnap -nt 001111 %f%", "/dfsmount/snapshots/*")
#
# for s in res: print s
#
# for s in res: print s.expandtabs(1).strip()
