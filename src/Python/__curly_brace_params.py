import re
import glob
import sys

def check_braces(pattern):

    brace_depth = 0

    for c in pattern:
        if c == "{":
            brace_depth += 1
        elif c == "}":
            brace_depth -= 1
        if brace_depth > 1 or brace_depth < 0:
            return False

    if brace_depth:
        return False

    return True


def parse_pattern(pattern):
    arguments = []
    blocks = []
    arg = ""
    block_number = 0

    for c in pattern:
        if c == "}":
            blocks.append(arg)
            arguments.append(block_number) # the string with this index in the blocks list is an {argument}
            arg = ""
            block_number += 1
        elif c == "{":
            blocks.append(arg)
            block_number += 1
            arg = ""
        else:
            arg += c

    block_types = []

    for block in blocks:
        if "*" in block:
           block_types.append((block, "v")) # variable string
        else:
           block_types.append((block, "c")) # constant string

    for i in range(len(block_types) - 1):
        if block_types[i][1] == "v" and block_types[i + 1][1] == "v":
            sys.exit("When using parameter capturing with {} braces, "
                     "wildcard characters * must not appear in two consecutive "
                     "sections of the pattern. There must be a unique constant "
                     "string between wildcards.")

    return block_types, arguments


def brace_replace(command_template, parsed_pattern, arguments, glob_result):

    params = []

    for idx in range(len(parsed_pattern)):
        text, block_type = parsed_pattern[idx]
        if block_type == "c":
            glob_result = glob_result[len(text):]
        if block_type == "v":
            if idx < len(parsed_pattern) - 1:
                nextblock = parsed_pattern[idx + 1][0]
                end_idx = glob_result.find(nextblock)
                text = glob_result[:end_idx]
                glob_result = glob_result[end_idx:]
            else:  # we've reached the end of the string
                text = glob_result
        if idx in arguments:
            params.append(text)

    files = re.findall("{f[0-9]+}", command_template)

    for f in files:
        fbrace = "<[" + f.replace("f", "") + "]>"
        command_template = command_template.replace(f, fbrace)

    # isolate the unique placeholders and sort them

    placeholders = re.findall("{[0-9]+}", command_template)

    placeholders = list(set(placeholders))

    placeholders.sort()

    if len(placeholders) > len(params):
        sys.exit("The number of unique parameters in the command template exceeds the number of parameters "
                 "captured with braces in the pattern.")

    for i, p in enumerate(placeholders):
        command_template = command_template.replace(p, params[i])

    return command_template


def parse_glob(command, pattern):

    if not check_braces(pattern):
        print ("There is a problem with the braces {...} in your filename pattern. "
               "Braces must be properly paired and their ranges cannot overlap.")
        return

    cleaned_pattern = pattern.replace("{", "").replace("}", "")

    results = glob.glob(cleaned_pattern)

    parsed_pattern, args = parse_pattern(pattern)

    return [brace_replace(command, parsed_pattern, args, result) for result in results]

