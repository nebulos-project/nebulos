/*
NebulOS: a Big Data framework for scientific data analysis.
Copyright (C) 2015 The Regents of the University of California

Developed by Nathaniel R. Stickley <nathaniel.stickley@gmail.com>
and Miguel A. Aragon-Calvo <miguel.angel.aragon.calvo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scheduler/nebulos_driver.hpp"

#include <boost/python.hpp>

typedef boost::python::list PyList;

/**
 * @brief ToPyList converts an iterable C++ container of value_type objects to a
 * Python list containing objects that are equivalent to value_type.
 * The container of type T should have a member type named T::value_type.
 * Furthermore, T::value_type should be a standard type (int, float, double,
 * long, string) in order for the Python conversion to work properly.
 * @param cpplist is an iterable C++ container.
 * @return a native Python list containing copies of the objects stored within
 * cpplist.
 */
template <typename T>
PyList ToPyList(const T& cpplist)
{
   PyList pylist;

   for (const typename T::value_type& entry : cpplist)
   {
      pylist.append(entry);
   }

   return pylist;
}

/**
 * @brief PyRun runs a simple batch job. The thread is blocked until
 * all tasks have finished.
 * @param _settings is a JobSettings objectspecifying the
 * characteristics of the batch job.
 * @param _commands is a string containing a list of commands to be
 * launched.
 * @return Returns a list containing the standard output of each task
 * (if the output == stdout in the _settings).
 */
PyList PyRun(const nebulos::JobSettings _settings,
                           const char* _commands)
{
    nebulos::Driver driver;

    return ToPyList(driver.RunBatch(_settings, _commands));
}

/**
 * @brief The Stream class allows a scheduler to become interactive. The
 * thread is not blocked when managed by this class
 *
 * The Stream class allows new tasks to be added to a sheduler while the
 * scheduler is running. It also allows the status of the scheduler, and
 * individual tasks assigned to the scheduler, to be queried.
 */
class Stream
{
public:

   Stream() {}

   Stream(const nebulos::JobSettings settings) { Init(settings); }

   ~Stream()
   {
      if (driver_) { delete driver_; }
   }

   void Init(const nebulos::JobSettings settings)
   {
      if (driver_ == nullptr)
      {
         driver_ = new nebulos::Driver(settings);
      }
   }

   /**
    * @brief BeginStreaming starts executing the stream. A scheduler
    * is created and the assigned tasks are assigned to the scheduler.
    * @param commands is a string containing a list of commands to be
    * added.
    */
   void BeginStreaming(const char* commands)
   {
      (driver_) ? driver_->CreateStream(commands) : DriverError();
   }

   /**
    * @brief Add adds new commands to the stream by assigning the tasks to
    * the scheduler.
    * @param commands is a string containing a list of commands to be
    * added.
    */
   void Add(const char* commands)
   {
      (driver_) ? driver_->AddToStream(commands) : DriverError();
   }

   /**
    * @brief Halt shuts down the whole framework (the scheduler and executors).
    * @return Returns true if the framework was sucessfully halted, false
    * otherwise.
    */
   bool Halt()
   {
      if (driver_)
      {
         delete driver_;
         driver_ = nullptr;
         return true;
      }

      return false;
   }

   /**
    * @brief Wait blocks the current thread until all tasks have completed.
    * @return Returns true if the stream was running and was successfully
    * waited upon. Otherwise, returns false.
    */
   bool Wait()
   {
      if (driver_ == nullptr)
      {
         std::cerr << "There is nothing to wait for.\n";
         return false;
      }

      const bool success = driver_->Wait();

      closed_ = true;

      return success;
   }

   bool KillTask(nebulos::taskid tid)
   {
      if (!driver_) { DriverError(); }

      return (driver_) ? driver_->KillTask(tid) : false;
   }

   /**
    * @brief closed tests whether the stream has been closed by Wait().
    * @return Returns true if the stream is closed, false otehrwise.
    */
   bool closed() const { return closed_; }

   /**
    * @brief GetResults collects the standard output of completed tasks.
    * @return Returns a list in which each entry contains the output of an
    * individual task.
    */
   PyList GetResults()
   {
      if (!driver_) { DriverError(); }

      return (driver_) ? ToPyList(driver_->GetResults()) : PyList();
   }

   /**
    * @brief TasksRemaining finds the number of tasks that have not yet
    * completed.
    * @return Returns the number of tasks assigned to this stream that have not
    * yet completed.
    */
   nebulos::ullint TasksRemaining() const
   {
      if (!driver_) { DriverError(); }

      return (driver_) ? driver_->TasksRemaining() : 0;
   }

   /**
    * @brief TasksCompleted finds the number of tasks thave have completed.
    * @return Returns the number of tasks submitted to this stream that have
    * completed, sucsessfully or unsucsessfully.
    */
   nebulos::ullint TasksCompleted() const
   {
      if (!driver_) { DriverError(); }

      return (driver_) ? driver_->TasksCompleted() : 0;
   }

   /**
    * @brief TasksLost finds the number of tasks that are currently in the
    * "lost" state.
    * @return Returns the number of tasks that are currently in the "lost"
    * state.
    */
   nebulos::ullint TasksLost() const
   {
      if (!driver_) { DriverError(); }

      return (driver_) ? driver_->TasksLost() : 0;
   }

   /**
    * @brief TasksRunning finds the number of tasks that are currently running.
    * @return Returns the number of tasks currently running.
    */
   uint TasksRunning() const
   {
      if (!driver_) { DriverError(); }

      return (driver_) ? driver_->TasksRunning() : 0;
   }

   /**
    * @brief TotalTasks finds the number of tasks that have been assigned
    * to this stream / scheduler.
    * @return Returns the number of tasks that have been assigned to this
    * stream.
    */
   nebulos::ullint TotalTasks() const
   {
      if (!driver_) { DriverError(); }

      return (driver_) ? driver_->TotalTasks() : 0;
   }

   /**
    * @brief LostTasks finds which tasks are currently lost.
    * @return Returns a list of task ID numbers belonging to tasks that are
    * currently lost. Tasks that have been relaunched and lost three times
    * without success are not included in this list.
    */
   PyList LostTasks() const
   {
      if (!driver_) { DriverError(); }

      return (driver_) ? ToPyList(driver_->LostTasks()) : PyList();
   }

   /**
    * @brief RunningTasks finds which tasks are currently running.
    * @return Returns a liost containing ID numbers of tasks that are currently
    * running.
    */
   PyList RunningTasks() const
   {
      if (!driver_) { DriverError(); }

      return (driver_) ? ToPyList(driver_->RunningTasks()) : PyList();
   }

   /**
    * @brief CompletedTasks finds the task ID number of tasks that have completed.
    * @return Returns a list with one task ID per entry.
    */
   PyList CompletedTasks() const
   {
      if (!driver_) { DriverError(); }

      return (driver_) ? ToPyList(driver_->CompletedTasks()) : PyList();
   }

   /**
    * @brief RemainingTasks finds the task ID numbers of tasks that have not yet
    * completed.
    * @return Returns a list with one task ID per entry.
    */
   PyList RemainingTasks() const
   {
      if (!driver_) { DriverError(); }

      return (driver_) ? ToPyList(driver_->RemainingTasks()) : PyList();
   }


   /**
    * @brief HostOfTask finds the host associated with the given task.
    * @param task_id is the ID number of the task in question.
    * @return Returns a string containing the name of the host currently
    * associated with the task in question. Returns an emply string if no host
    * is associated with the task or the task does not exist.
    *
    * HostOfTask finds the host associated with the given task. Refer to
    * NebulOSScheduler::HostOfTask() for more details.
    */
   std::string HostOfTask(const nebulos::taskid task_id) const
   {
      if (!driver_) { DriverError(); }

      if (driver_)
      {
          const std::string* host = driver_->HostOfTask(task_id);
          return (host) ? *host : "";
      }
      else
      {
         return "";
      }
   }

   /**
    * @brief Command finds the command associatied with a particular task ID.
    * @param task_id is the task ID number of the task in question.
    * @return Returns a string containing the command associated with
    * the specified task ID.
    */
   std::string Command(const nebulos::taskid task_id) const
   {
      if (!driver_) { DriverError(); }

      return (driver_) ? driver_->Command(task_id) : "";
   }

   PyList Find(const std::string pattern) const
   {
      if (!driver_) { DriverError(); }

      return (driver_) ? ToPyList(driver_->Find(pattern)) : PyList();
   }

   /**
    * @brief Status reports the status of the specified task.
    * @param task_id is the task ID number of the task in question.
    * @return Returns the status of the task in question.
    */
   std::string Status(const nebulos::taskid task_id) const
   {
      if (!driver_) { DriverError(); }

      return (driver_) ? driver_->Status(task_id) : "";
   }

private:

   void DriverError() const
   {
      std::cerr << "Error: NebulOS Driver has not been created.\n";
   }

   nebulos::Driver* driver_ {nullptr};
   bool closed_ {false};
};


void CleanUp()
{
   std::cerr << "This is where the cleanup code goes.\n";
}

// this instructs Boost::Python to work some magic. The end result is that
// the code below exposes C++ code to Python.
BOOST_PYTHON_MODULE(__nebulos)
{
   using namespace boost::python;

   class_<nebulos::JobSettings>("__settings")
   .def_readwrite("mode", &nebulos::JobSettings::mode)
   .def_readwrite("master", &nebulos::JobSettings::master)
   .def_readwrite("role", &nebulos::JobSettings::role)
   .def_readwrite("threads_per_task", &nebulos::JobSettings::threads_per_task)
   .def_readwrite("mem_per_task", &nebulos::JobSettings::mem_per_task)
   .def_readwrite("disk_per_task", &nebulos::JobSettings::disk_per_task)
   .def_readwrite("monitor", &nebulos::JobSettings::monitor)
   .def_readwrite("relauncher", &nebulos::JobSettings::relauncher)
   .def_readwrite("update_interval", &nebulos::JobSettings::update_interval)
   .def_readwrite("output_target", &nebulos::JobSettings::output_target)
   .def_readwrite("log_directory", &nebulos::JobSettings::log_directory)
   .def_readwrite("job_id", &nebulos::JobSettings::job_id)
   .def_readwrite("verbosity", &nebulos::JobSettings::verbosity);

   def("__run", PyRun);

   class_<Stream>("__stream", init<nebulos::JobSettings>())
         .def("get_results", &Stream::GetResults)
         .def("wait", &Stream::Wait)
         .def("halt", &Stream::Halt)
         .def("init", &Stream::Init)
         .def("add", &Stream::Add)
         .def("kill_task", &Stream::KillTask)
         .def("begin_streaming", &Stream::BeginStreaming)
         .def("closed", &Stream::closed)
         .def("count_unfinished_tasks", &Stream::TasksRemaining)
         .def("count_completed_tasks", &Stream::TasksCompleted)
         .def("count_lost_tasks", &Stream::TasksLost)
         .def("count_running_tasks", &Stream::TasksRunning)
         .def("count_total_tasks", &Stream::TotalTasks)
         .def("list_lost_tasks", &Stream::LostTasks)
         .def("list_running_tasks", &Stream::RunningTasks)
         .def("list_completed_tasks", &Stream::CompletedTasks)
         .def("list_unfinished_tasks", &Stream::RemainingTasks)
         .def("get_command", &Stream::Command)
         .def("get_hostname", &Stream::HostOfTask)
         .def("search_tasks", &Stream::Find)
         .def("get_status", &Stream::Status);

   scope().attr("__cleanup") = CleanUp;
}
