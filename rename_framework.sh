#! /bin/bash

if [[ "$#" -ne 2 ]]; then
	echo "usage: $ rename_framework.sh old_name new_name"
	exit
fi

OLD=$1
NEW=$2

# rename files from variations of $OLD to equivalent variations of $NEW, 
# while presrving the case style (e.g., Old --> New, old --> new, OLD --> NEW)

# when the file names are exactly as typed by the user:

files=$(find . -name "*$OLD*")

for file in $files; do
	mv $file ${file/$OLD/$NEW}
done

# when all letters are lower-case:

files=$(find . -name "*${OLD,,}*")

for file in $files; do
	mv $file ${file/${OLD,,}/${NEW,,}} 
done

# when using mixed case (proper noun) capitalization:

files=$(find . -name "*${OLD^}*")

for file in $files; do
	mv $file ${file/${OLD^}/${NEW^}}
done

# when all letters are upper-case:

files=$(find . -name "*${OLD^^}*")

for file in $files; do
	mv $file ${file/${OLD^^}/${NEW^^}} 
done

# modify file contents in the same way as the file names were changed

sed -i "s/$OLD/$NEW/g" $(find . -type f -name "*")

sed -i "s/${OLD,,}/${NEW,,}/g" $(find . -type f -name "*")

sed -i "s/${OLD^}/${NEW^}/g" $(find . -type f -name "*")

sed -i "s/${OLD^^}/${NEW^^}/g" $(find . -type f -name "*")